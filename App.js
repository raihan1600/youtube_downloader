import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import Rooter from '@services/rooter';
import * as eva from '@eva-design/eva';
import { default as mapping } from '@assets/custom/custom-maping.json';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { store } from '@services/store';
import { Provider } from 'react-redux';
import TrackPlayer, {Capability} from "react-native-track-player";
import MainService from '@services/main';

const RNFS = require('react-native-fs');

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            theme: 'light',
        };
    }

    async componentDidMount(){
        MainService.openDb();

        RNFS.readDir(`${RNFS.DocumentDirectoryPath}/Thumbnails`).then(resDir =>{
            console.log("Folder created");
        }).catch(err =>{
            RNFS.mkdir(`${RNFS.DocumentDirectoryPath}/Thumbnails`);
            RNFS.mkdir(`${RNFS.DocumentDirectoryPath}/Videos`);
            RNFS.mkdir(`${RNFS.DocumentDirectoryPath}/Audios`);
            RNFS.mkdir(`${RNFS.DocumentDirectoryPath}/Frames`);
            RNFS.mkdir(`${RNFS.DocumentDirectoryPath}/Video-Editor`);
            RNFS.mkdir(`${RNFS.DocumentDirectoryPath}/www`);
        });

        TrackPlayer.updateOptions({
			capabilities: [
				Capability.Play,
				Capability.Pause,
				Capability.SkipToNext,
				Capability.SkipToPrevious,
				Capability.Stop,
			],
			compactCapabilities: [
                Capability.Play, 
                Capability.Pause
            ],
			stopWithApp: false
		});
    }


    render() {
        return (
            <>
                <IconRegistry icons={EvaIconsPack} />
                <Provider store={store}>
                    <ApplicationProvider {...eva} theme={eva.light} customMapping={mapping}>
                        <SafeAreaView style={{flex: 1}}>
                            <Rooter />
                        </SafeAreaView>
                    </ApplicationProvider>
                </Provider>
            </>  
        );
    }
}

export default App;