var baseUrl = window.location.href;
// var baseUrl = "http://192.168.1.29:4000/";
var app = new Vue({
	el: '#app',
	data: {
		message: 'Hello Vue!',
		dataFile: []
	},
	updated: function(){
		// console.log(this.dataFile);
	},
	mounted: function () {
		let $this = this;
		
		// ============================ //
		// REGISTER DEVICE
		// ============================ //
		axios.get(baseUrl + 'register').then(function (response) {
			// console.log(response);
		}).catch(function (error) {
			// console.log(error);
		});

		// ============================ //
		// DRAG DROP FUNCTION
		// ============================ //
		var drop = $("input");
		drop.on('dragenter', function (e) {
			$(".drop").css({
				"border": "4px dashed #09f",
				"background": "rgba(0, 153, 255, .05)"
				});
			$(".cont").css({
				"color": "#09f"
			});
		}).on('dragleave dragend mouseout drop', function (e) {
			$(".drop").css({
				"border": "3px dashed #DADFE3",
				"background": "transparent"
			});
			$(".cont").css({
				"color": "#8E99A5"
			});
		});

		// ============================ //
		// ON File Change
		// ============================ //
		$('#files').change(async (e)=>{
			let files = e.target.files;
			for(let i=0; i<files.length;i++){
				files[i]['index'] = i;
				files[i]['file'] = await this.toBase64(files[i]);
				this.dataFile.push(files[i]);
			}

			axios.get(baseUrl + 'add-upload?total=' + files.length).then((resQueue)=>{
				this.dataFile.map((x)=>{
					axios.get(baseUrl + 'upload',{
						headers: {
							'file': x['file'],
							'name': x['name'],
							'size': x['size'],
							'type': x['type']
						}
					}).then( (response)=> {
						console.log("response upload",response);
						$('#files').val('');
						this.dataFile = this.dataFile.filter(item => item.index != x.index);
					}).catch((error) =>{
						// console.log(error);
					});
				});
			});
		});
	},
	methods: {
		toBase64: function (file) {
			return new Promise((resolve, reject) => {
				const reader = new FileReader();
				reader.readAsDataURL(file);
				reader.onload = () => resolve(reader.result);
				reader.onerror = error => reject(error);
			});
		}
	}
});