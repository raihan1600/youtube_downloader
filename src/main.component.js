import React, { Component } from 'react';
import {
  SafeAreaView,
} from 'react-native';
import AudioPlaylistComponent from '@pages/audio/audio-playlist.component';
import FolderComponent from '@pages/folder/folder.component';
import PorfileLoginComponent  from '@pages/profile/profile-login.component';
import VideoEditorComponent from "@pages/video-editor/video-editor.component";
import Menucomponent from "./pages/menu/menu.component";
import PlaylistStore from "@components/playlist.store";
import Feather from 'react-native-vector-icons/Feather';
import { Layout, BottomNavigation, BottomNavigationTab, Divider } from '@ui-kitten/components';
import mainStyles from "@services/style";
const color = require('@assets/custom/custom-maping.json');

class MainComponent extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tabIndex: 0
		};
	}
  	render(){
		let content = null;
		switch(this.state.tabIndex){
			case 0 :
				content = <Menucomponent navigation={this.props.navigation}/>;
			break;
			case 1 :
				content = <AudioPlaylistComponent navigation={this.props.navigation}/>;
			break;
			case 2 :
				content = <VideoEditorComponent navigation={this.props.navigation}/>;
			break;
			case 3 :
				content = <PorfileLoginComponent navigation={this.props.navigation}/>;
			break;
		}

		return(
			<SafeAreaView style={mainStyles.container}>
				<PlaylistStore />
				<Layout style={{flex:1}}>
					{content}
				</Layout>
				<Divider />
				<BottomNavigation 
					indicatorStyle={{height:0}}
					selectedIndex={this.state.tabIndex}
					onSelect={(e)=> this.setState({tabIndex:e})}
				>
					<BottomNavigationTab title='Home' icon={()=> <Feather name='home' size={20} color={this.state.tabIndex === 0 ? color['color-primary-500'] : '#3A3A3A'} /> }/>
					<BottomNavigationTab title='Player' icon={()=> <Feather name='music' size={20} color={this.state.tabIndex === 1 ? color['color-primary-500'] : '#3A3A3A'}/> }/>
					<BottomNavigationTab title='Editor' icon={()=> <Feather name='film' size={20} color={this.state.tabIndex === 2 ? color['color-primary-500'] : '#3A3A3A'} /> }/>
					<BottomNavigationTab title='PRO' icon={()=> <Feather name='gift' size={20} color={this.state.tabIndex === 3 ? color['color-primary-500'] : '#3A3A3A'} /> }/>
				</BottomNavigation>
			</SafeAreaView>
		)
	}
};


export default MainComponent;
