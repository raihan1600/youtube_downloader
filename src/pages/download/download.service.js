import { RNFFmpeg,RNFFprobe, RNFFmpegConfig } from 'react-native-ffmpeg';
import kebabCase from 'lodash/kebabCase';
import { store } from '@services/store';
import MainService from '@services/main';
import CameraRoll from "@react-native-community/cameraroll";
const RNFS = require('react-native-fs');
console.log(RNFS.DocumentDirectoryPath);

const downloadService = {
    downloadDuration: 0,

    // ================================= //
    // FFMPEG PROCESS
    // ================================= //
    getFFMPEGLog: ()=>{
        RNFFmpegConfig.enableLogCallback((e)=>{
            let getTime = e.message.match(/time=(.*) bitrate/) === null ? e.message.match(/time= (.*) bitrate/) : e.message.match(/time=(.*) bitrate/);
            if(getTime !== null){
                let current = downloadService.timeString2ms(getTime[1]);
                let setpercentage = Math.ceil((current / downloadService.downloadDuration) * 100);

                store.dispatch({ 
                    type: "UPDATE_DOWNLOAD_STATUS", 
                    downloadStatus: "Extracting..."
                });	
                store.dispatch({ 
                    type: "UPDATE_DOWNLOAD_PROGRESS", 
                    downloadProgress: setpercentage
                });	
                store.dispatch({ 
                    type: "UPDATE_DOWNLOAD_LOADING", 
                    downloadLoading: true
                });	
            }
        });
    },

    // ================================= //
    // GET MEDIA INFORMATION 
    // ================================= //
    getInformation: (url) =>{
        return new Promise((resolve,reject)=>{
            RNFFprobe.getMediaInformation(url).then(information => {
                resolve(information);
            })
        });
    },

    // ========================= //
    // Time to String
    // ========================= //
    timeString2ms: function(time){
        var arr = time.split(':');
        var hh = arr[0] * 60 * 60;
        var mm = arr[1] * 60;
        var ss = arr[2];
        return Number(hh) + Number(mm) + Math.trunc(Number(ss));
    },

    // ================================= //
    // DOWNLOAD FILE FROM URL
    // ================================= //
    downloadFileFromURL: (url,urlThumbnail,type,mediaData,title,thumbnailType,isMP3Only) =>{
        return new Promise((resolve,reject)=>{
            downloadService.downloadDuration = Number(mediaData.duration);
            let randString = MainService.makeid(10);
            let toFileMedia = `${RNFS.DocumentDirectoryPath}/${type}/${kebabCase(title)}-${randString}`;
            let size = 0;
            RNFS.downloadFile({
                fromUrl: url,
                toFile: `${toFileMedia}.${isMP3Only ? "mp3" : "mp4"}`,
                background: true,
                progressInterval: 1,
                begin: (res)=>{
                    size = res['contentLength']
                },
                progress: (res)=>{
                    let valProgress = (100 * res['bytesWritten']) / res['contentLength'];
                    store.dispatch({ 
                        type: "UPDATE_DOWNLOAD_PROGRESS", 
                        downloadProgress: valProgress
                    });	
                }
            }).promise.then(res =>{	
                resolve();

                if(type === 'Audios' && !isMP3Only){
                    let json = mediaData;
                    json['path'] =  `${toFileMedia}.mp3`;
                    downloadService.getFFMPEGLog();

                    RNFFmpeg.executeWithArguments(['-i' ,`${toFileMedia}.mp4`, '-acodec', 'libmp3lame',`${toFileMedia}.mp3`]).then(result =>{
                        RNFS.unlink(`${toFileMedia}.mp4`).then(() => {
                            let toFileThumb = "";
                            if(urlThumbnail){
                                toFileThumb = `${RNFS.DocumentDirectoryPath}/Thumbnails/${kebabCase(title)}-${randString}.jpg`;
                                downloadService.downloadThumbnail(urlThumbnail,toFileThumb,thumbnailType);
                            }
                            json['thumbnail'] = `${toFileThumb}`;
                            json['content_length'] = size;
                            downloadService.writeFileData(type, json);

                            store.dispatch({ 
                                type: "UPDATE_DOWNLOAD_PROGRESS", 
                                downloadProgress: 0
                            });	
                            store.dispatch({ 
                                type: "UPDATE_DOWNLOAD_LOADING", 
                                downloadLoading: false
                            });	
                        });
                    });
                }else{
                    CameraRoll.save(`file://${toFileMedia}.mp4`, { type: "video", album: "Medova" }).then(resSave =>{
                        RNFS.unlink(`${toFileMedia}.mp4`).then(() => {
                            store.dispatch({ 
                                type: "UPDATE_DOWNLOAD_PROGRESS", 
                                downloadProgress: 0
                            });	
                            store.dispatch({ 
                                type: "UPDATE_DOWNLOAD_LOADING", 
                                downloadLoading: false
                            });	
                        });
                    });
                }
            });
        });
    },

    // ================================= //
    // DOWNLOAD THUMBNAIL
    // ================================= //
    downloadThumbnail: (url,toFileThumb,thumbnailType) =>{
        return new Promise((resolve,reject)=>{
            if(thumbnailType == "url"){
                RNFS.downloadFile({
                    fromUrl: url,
                    toFile: toFileThumb,
                    background: true,
                    progressInterval: 1,
                    begin: (res)=>{
                    },
                    progress: (res)=>{
                        let valProgress = (100 * res['bytesWritten']) / res['contentLength'];
                    }
                }).promise.then(res =>{
                    resolve();
                });
            }else{
                RNFS.copyFile(url,toFileThumb).then(res =>{
                    resolve();
                });
            }
        });
    },
    
    // ================================= //
    // WRITE FILE DATA WHEN DOWNLOAD
    // ================================= //
    writeFileData: async(type,json)=> {
		MainService.realmDb.write(() => {
			MainService.realmDb.create(type, {
				_id: MainService.makeid(25),
				...json
			});
		});
        store.dispatch({ type: "UPDATE_UID_TRACK", updateUIDTrack: MainService.makeid(25)});
	},

    // ================================= //
    // CREATE THUMBNAIL FROM VIDEO
    // ================================= //
    createThumbnail: async(url)=> {
		return new Promise((resolve,reject)=>{
            RNFFmpeg.executeWithArguments(['-i' ,`${url}`, '-ss', '00:00:01.000',`-vframes`,'1',`${RNFS.DocumentDirectoryPath}/output_thumb.png`]).then(result =>{
				resolve(`${RNFS.DocumentDirectoryPath}/output_thumb.png`);
			});
        });
	}
};

export default downloadService;