export const downloadReducer = (state = {
	downloadData: null,
	downloadProgress: 0,
	downloadLoading: false,
	downloadURL: null,
	downloadType: 'Videos',
	downloadStatus: 'Downloading...'
}, action) => {
	switch (action.type) {
		case "UPDATE_DOWNLOAD_STATUS":
			state = {
				...state,
				downloadStatus: action.downloadStatus
			}
        break;
		case "UPDATE_DOWNLOAD_DATA":
			state = {
				...state,
				downloadData: action.downloadData
			}
        break;
		case "UPDATE_DOWNLOAD_URL_TYPE":
			state = {
				...state,
				downloadURL: action.downloadURL,
				downloadType: action.downloadType,
			}
        break;
		case "UPDATE_DOWNLOAD_PROGRESS":
			state = {
				...state,
				downloadProgress: action.downloadProgress,
			}
        break;
		case "UPDATE_DOWNLOAD_LOADING":
			state = {
				...state,
				downloadLoading: action.downloadLoading,
			}
        break;
	}
	return state;
}