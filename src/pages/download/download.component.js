import React, { Component } from 'react';
import { View ,Image, Dimensions, ActivityIndicator,ScrollView } from 'react-native';
import { 
	Button, 
	Divider, 
	Icon, 
	Input, 
	Layout,
	Text, 
	TopNavigation, 
	Toggle
} from '@ui-kitten/components';
import SnackBar from 'react-native-snackbar-component';
import { store } from '@services/store';
import { connect } from 'react-redux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import styles from './download.style';
import ytdl from "react-native-ytdl";
import isEmpty from "lodash/isEmpty";
import downloadService from "./download.service";
import axios from 'axios';
import mainStyles from '@services/style';
const CryptoJS = require("crypto-js");
const windowWidth = Dimensions.get('window').width;
const host = "https://medova.anurdin.net";
const secretKey = "Welcome@123!";
const color = require('@assets/custom/custom-maping.json');

class DownloadComponent extends Component {
  constructor(props) {
		super(props);
		this.state = {
			type: 'Videos',
			AudioChecked: false,
			VideoChecked: true,
			AudioOnly: false,
			VideoOnly: false,
			DurationStart: 0,
			DurationEnd: 100,
			valProgress: 0,
			progressGetDownload: false,
			progressFileDownload: false,
			info: null,
			showAlert: false,
			Dropdown:'',
			urlDownload: '',
			alertText: "",
			alertBackgroundColor: "",
			toFileMedia: "",
			toFileThumb: "",
			alertMessageColor:"#FFFFFF",
			isMP3Only: false
		};
	}

	componentDidMount(){
		if(this.props.route.params){
			console.log(this.props.route.params);
			this.setState({
				urlDownload: this.props.route.params.urlDownload,
				type: this.props.route.params.downloadType,
				AudioChecked: this.props.route.params.downloadType == "Audios",
				VideoChecked: this.props.route.params.downloadType == "Videos"
			},()=>{
				this.getDownload();
			});
		}else{
			if(this.props.download){
				this.setState({
					urlDownload: this.props.downloadUrl,
					type: this.props.downloadType,
					AudioChecked: this.props.downloadType == "Audios",
					VideoChecked: this.props.downloadType == "Videos"
				});
			}
		}
	}

	// ====================== //
	// GET DOWNLOADN
	// ====================== //
	getDownload(){
		this.setState({
			progressGetDownload: true,
			VideoOnly: false,
			AudioOnly: false
		});
		store.dispatch({ 
			type: "UPDATE_DOWNLOAD_PROGRESS", 
			downloadProgress: 0
		});	
		let obj;

		// =============================== //
		// Download Souncloud
		// =============================== //
		if(this.state.urlDownload.includes("soundcloud.com")){
			console.log("soundcloud");
		}

		// =============================== //
		// Download Facebook
		// =============================== //
		if(this.state.urlDownload.includes("facebook.com")){
			axios.get(`${host}/download-fb?url=${this.state.urlDownload}`).then((res) =>{
				let bytes  = CryptoJS.AES.decrypt(res.data, secretKey);
				let decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
				let url_download = decryptedData.url_hd ? decryptedData.url_hd : decryptedData.url_sd;
				downloadService.getInformation(url_download).then(info =>{
					let media = {
						"title": decryptedData['title'],
						"category": "",
						"category_url": "",
						"artist": "",
						"artist_url": "",
						"duration": info.getMediaProperties().duration,
						"licensed": this.state.urlDownload
					};
					obj = {
						download: decryptedData.url_hd ? decryptedData.url_hd : decryptedData.url_sd,
						title: decryptedData.title,
						thumbnail: decryptedData.thumbnail,
						thumbnailType: 'url',
						info: "",
						media: media
					};
					this.setState({progressGetDownload: false, isMP3Only: false});
					store.dispatch({ 
						type: "UPDATE_DOWNLOAD_DATA", 
						downloadData: obj
					});	
				});
			}).catch(err =>{
				this.handleErrorGetURL();
			});
		} 

		// =============================== //   
		// Download TikTok
		// =============================== //
		if(this.state.urlDownload.includes("tiktok.com")){
			axios.get(`${host}/download-tiktok?url=${this.state.urlDownload}`).then(res =>{
				let bytes  = CryptoJS.AES.decrypt(res.data, secretKey);
				let decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
				downloadService.getInformation(decryptedData.video).then(info =>{
					let media = {
						"title": decryptedData['title'],
						"category": "",
						"category_url": "",
						"artist": "",
						"artist_url": "",
						"duration": info.getMediaProperties().duration,
						"licensed": this.state.urlDownload
					};
					obj = {
						download: decryptedData.video,
						title: decryptedData.title,
						thumbnail: decryptedData.thumbnail,
						thumbnailType: 'url',
						info: decryptedData.desc,
						media: media
					};
					this.setState({progressGetDownload: false, isMP3Only: false});
					store.dispatch({ 
						type: "UPDATE_DOWNLOAD_DATA", 
						downloadData: obj
					});	
				});
			}).catch(err =>{
				this.handleErrorGetURL();
			});
		}

		// =============================== //
		// Download Youtube
		// =============================== //
		if(this.state.urlDownload.includes("youtube.com") || this.state.urlDownload.includes("youtu.be")){
			const option = { filter: format => format.container === 'mp4' };
			const youtubeURL = `https://www.youtube.com/watch?v=${this.youtube_pharser()}`;
			Promise.all([ytdl(youtubeURL, option),ytdl.getInfo(this.youtube_pharser())]).then(resData =>{
				let dataURL = resData[0][0];
				let info = resData[1];
				let media;
				if(isEmpty(info.videoDetails.media)){
					media = {
						"title": info.videoDetails['title'],
						"category": "",
						"category_url": "",
						"artist": info.videoDetails['ownerChannelName'],
						"artist_url": info.videoDetails['ownerProfileUrl'],
						"duration":info.videoDetails['lengthSeconds'],
						"licensed": info.videoDetails['video_url']
					};
				}else{
					media = {
						"title": info.videoDetails.media['song'],
						"category": info.videoDetails.media['category'],
						"category_url": info.videoDetails.media['category_url'],
						"artist": info.videoDetails.media['artist'],
						"artist_url": info.videoDetails.media['artist_url'],
						"duration":info.videoDetails['lengthSeconds'],
						"licensed": info.videoDetails.media['video_url']
					};
				}
				obj = {
					download: dataURL.url,
					title: info.videoDetails.title,
					thumbnail: info.videoDetails.thumbnails[info.videoDetails.thumbnails.length-1].url,
					thumbnailType: 'url',
					info: info.videoDetails.description,
					media: media
				};
				this.setState({progressGetDownload: false, isMP3Only: false});
				store.dispatch({ 
					type: "UPDATE_DOWNLOAD_DATA", 
					downloadData: obj
				});	
			}).catch(err =>{
				this.handleErrorGetURL();
			});
		}

		// =============================== //
		// Download MP3
		// =============================== //
		let detectMP3 = /\.(mp3)$/i.test(this.state.urlDownload);
		if(detectMP3 || this.props.route.params?.extension == "mp3"){
			let filename = this.state.urlDownload.substring(this.state.urlDownload.lastIndexOf('/')+1);
			downloadService.getInformation(this.state.urlDownload).then(info =>{
				console.log(info);
				if(info.getMediaProperties()){
					this.setState({
						VideoOnly: false,
						AudioOnly: true,
						VideoChecked: false, 
						AudioChecked: true,
						type: "Audios",
						progressGetDownload: false,
						isMP3Only: tru
					});

					let media = {
						"title": filename,
						"category": "",
						"category_url": "",
						"artist": "",
						"artist_url": "",
						"duration": info.getMediaProperties().duration,
						"licensed": this.state.urlDownload
					};
					obj = {
						download: this.state.urlDownload,
						title: filename,
						thumbnail: "",
						thumbnailType: 'local',
						info: "",
						media: media
					};
					store.dispatch({ 
						type: "UPDATE_DOWNLOAD_DATA", 
						downloadData: obj
					});
				}else{
					this.handleErrorGetURL();
				}
			}).catch(err =>{
				console.log(err);
			})
		}

		// =============================== //
		// Download MP4
		// =============================== //
		let detectMP4 = /\.(mp4)$/i.test(this.state.urlDownload);
		if(detectMP4 || this.props.route.params?.extension == "mp4"){
			let filename = this.state.urlDownload.substring(this.state.urlDownload.lastIndexOf('/')+1);
			downloadService.getInformation(this.state.urlDownload).then(info =>{
				if(info.getMediaProperties()){
					downloadService.createThumbnail(this.state.urlDownload).then(res =>{
						this.setState({
							VideoChecked: true, 
							AudioChecked: false,
							type: "Videos",
							progressGetDownload: false,
							isMP3Only: false
						});
		
						let media = {
							"title": filename,
							"category": "",
							"category_url": "",
							"artist": "",
							"artist_url": "",
							"duration": info.getMediaProperties().duration,
							"licensed": this.state.urlDownload
						};
						obj = {
							download: this.state.urlDownload,
							title: filename,
							thumbnail: res,
							thumbnailType: 'local',
							info: "",
							media: media
						};
						store.dispatch({ 
							type: "UPDATE_DOWNLOAD_DATA", 
							downloadData: obj
						});
					});
				}else{
					this.handleErrorGetURL();
				}
			});
		}
	}

	handleErrorGetURL(){
		store.dispatch({ 
			type: "UPDATE_DOWNLOAD_DATA", 
			downloadData: null
		});	
		this.setState({
			progressGetDownload: false,
			showAlert: true,
			alertText: "File not found",
			alertBackgroundColor: color['color-danger-500']
		}, ()=>{
			setTimeout(()=>{
				this.setState({showAlert: false});
			}, 2000);
		});
	}

	// ====================== //
	// DOWNLOAD FILE
	// ====================== //
	downloadFileFromURL(url){
		store.dispatch({ 
			type: "UPDATE_DOWNLOAD_STATUS", 
			downloadStatus: "Downloading..."
		});	
		store.dispatch({ 
			type: "UPDATE_DOWNLOAD_LOADING", 
			downloadLoading: true
		});	
		store.dispatch({ 
			type: "UPDATE_DOWNLOAD_URL_TYPE", 
			downloadURL: this.state.urlDownload,
			downloadType: this.state.type,
		});	
		downloadService.downloadFileFromURL(
			url,
			this.props.download.thumbnail,
			this.state.type,
			this.props.download.media,
			this.props.download.title,
			this.props.download.thumbnailType,
			this.state.isMP3Only
		).then(res=>{
			this.setState({
                progressGetDownload: false,
                showAlert: true,
				alertText: "Download media success",
				alertBackgroundColor: color['color-success-700']
            });
		});
	}

	// ====================== //
	// Filter ID URL Youtube
	// ====================== //
	youtube_pharser() {
		var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
		var match = this.state.urlDownload.match(regExp);
		return match && match[7].length == 11 ? match[7] : false;
	}

	render() {
		return (
			<>
				<TopNavigation 
					title={()=> <Text category='s1'>Download</Text>}
					alignment='center'
					accessoryLeft={()=> (
						<MaterialIcons 
							color='#3A3A3A' 
							name='arrow-back' 
							size={28}
							onPress={()=> this.props.navigation.goBack()}
							style={mainStyles.iconTopNavigation}
						/>
					)}
				/>
				<Divider />
				<Layout style={{flex:1}}>
					<ScrollView 
						showsVerticalScrollIndicator={false} 
						contentContainerStyle={{ flexGrow: 1 }} 
						keyboardShouldPersistTaps="always"
						bounces={false}>
							{!this.props.route.params ? 
							<Layout style={{paddingTop:15,paddingHorizontal:15}}>
								<Image source={require('../../../assets/images/logo-brand.png')} style={{width:158,height:25}}/>
							</Layout>
							: null}
							<Layout style={{flexDirection:'row', paddingHorizontal:15,paddingBottom:15,paddingTop:10}}>
								<Input 
									style={{width:windowWidth-80,borderTopRightRadius:0,borderBottomRightRadius:0}}
									disabled={this.state.progressGetDownload || this.props.loading}
									defaultValue={this.state.urlDownload}
									onChangeText={(x)=> this.setState({urlDownload: x})}
									placeholder="Enter URL"/>
								<Button 
									style={{width:51,height:39,borderTopLeftRadius:0,borderBottomLeftRadius:0}} 
									disabled={this.state.progressGetDownload || this.props.loading}
									accessoryRight={this.state.progressGetDownload ? <ActivityIndicator size="small" /> : <Icon name="search" color="white"/>}
									onPress={()=>this.getDownload()}
								/>					
							</Layout>
							<Divider />
							<Layout style={{paddingVertical:10,paddingHorizontal:15}}>
								<View style={{flexDirection:'row'}}>
									<View style={{paddingRight:5}}>
										<Toggle 
											checked={this.state.VideoChecked} 
											disabled={this.state.progressGetDownload || this.props.loading || !this.props.download || this.state.AudioOnly}
											onChange={(e)=>{
												if(e){
													this.setState({
														VideoChecked: true, 
														AudioChecked: false,
														type: "Videos"
													});
												}else{
													this.setState({
														AudioChecked: true, 
														VideoChecked: false,
														type: "Audios"
													});
												}
											}}>
											<Text>Video</Text>
										</Toggle>
									</View>
									<View style={{paddingLeft:5}}>
										<Toggle 
											checked={this.state.AudioChecked} 
											disabled={this.state.progressGetDownload || this.props.loading || !this.props.download || this.state.VideoOnly}
											onChange={(e)=> {
												if(e){
													this.setState({
														AudioChecked: true, 
														VideoChecked: false,
														type: "Audios"
													});
												}else{
													this.setState({
														VideoChecked: true, 
														AudioChecked: false,
														type: "Videos"
													});
												}
											}}>
											Audio
										</Toggle>
									</View>
								</View>
							</Layout>
							<Divider />
							{this.props.download ? 
								<Layout style={{flex: 1}}>
									<Layout style={{paddingVertical:10, paddingHorizontal:15}}>
										<Text category="h6" numberOfLines={2}>{this.props.download.title}</Text>
										{this.props.download.info ? 
										<Text style={{marginTop:5}} numberOfLines={2}>{this.props.download.info}</Text>
										: null}
									</Layout>
									<Divider />
									{this.props.download.thumbnail ? 
									<>
									<Layout style={{justifyContent:'center',padding:15, backgroundColor:"#F5F5F5"}}>
										<Image 
											style={[styles.downloadPlayback,{width:windowWidth-30,height:((windowWidth-30)/2)}]} 
											source={this.props.download.thumbnailType == "url" ? {uri: this.props.download.thumbnail} : {uri: `file://${this.props.download.thumbnail}`}}/>
									</Layout>
									<Divider />
									</>
									: null}
									<View style={{backgroundColor:'#EFEFEF'}}>
										<View style={{
											height:4,
											width:windowWidth*(this.props.progress/100),
											backgroundColor:color['color-primary-500']
										}}/>
									</View>
									<Layout style={{
										padding:15,
										flex: 1,
										flexDirection: "row"
									}}>
										<Layout style={{ paddingRight: 7.5, flex: 1 }}>
											<Button 
												disabled={this.props.loading}
												onPress={()=>{
													this.downloadFileFromURL(this.props.download.download);
												}}>
												{this.props.loading ? this.props.downloadStatus : "Download"}
											</Button>
										</Layout>
										<Layout style={{ paddingLeft: 7.5, flex: 1 }}>
											<Button 
												status='basic'
												disabled={this.props.loading}
												onPress={()=>{
													this.setState({
														VideoChecked: true, 
														AudioChecked: false,
														type: "Videos",
														urlDownload: ''
													});
													store.dispatch({ 
														type: "UPDATE_DOWNLOAD_URL_TYPE", 
														downloadURL: null,
														downloadType: 'Videos',
													});	
													store.dispatch({ 
														type: "UPDATE_DOWNLOAD_DATA", 
														downloadData: null
													});
												}}>
												Clear
											</Button>
										</Layout>
									</Layout>
								</Layout>
							:
								<Layout style={{justifyContent:'center',paddingVertical:75}}>
									<Image style={[styles.downloadPlayback,{width:200,height:120}]} source={require('@assets/images/download.png')}/>
									<Text style={{textAlign:"center", marginTop:15}}>Download video or audio</Text>
								</Layout>
							}
					</ScrollView>
				</Layout>
				<SnackBar 
					visible={this.state.showAlert} 
					messageColor={this.state.alertMessageColor}
					accentColor={this.state.alertMessageColor}
					textMessage={this.state.alertText}
					backgroundColor={this.state.alertBackgroundColor}
					actionHandler={()=>this.setState({showAlert: false})} 
					actionText="OK"
				/>
			</>
		)
	}
}

function mapStateToProps(state) {
    return {
        init: state.init,
        download: state.download.downloadData,
        progress: state.download.downloadProgress,
        loading: state.download.downloadLoading,
		downloadUrl: state.download.downloadURL,
		downloadType: state.download.downloadType,
		downloadStatus: state.download.downloadStatus,
    }
}
export default connect(mapStateToProps, null, null, { forwardRef: true })(DownloadComponent);