import {StyleSheet, Dimensions} from 'react-native';

const styles = StyleSheet.create({
    imagePantool: {
        height: 139,
        width: 167,
        alignSelf: 'center',
        marginBottom: 40,
    },
    downloadPlayback:{
        alignSelf:'center',
        borderRadius:5
    },    
    duration: {
        backgroundColor: '#F7F9FC',
        borderColor: '#E4E9F2',
        width: 51,
        height: 25,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        borderRadius: 3,
    },
    markerslide: {
        height: 18,
        width: 18,
        backgroundColor: '#FFFFFF',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4.84,
        elevation: 5,
    },
});

export default styles;
