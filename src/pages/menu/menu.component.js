import React, { Component } from 'react'
import { TouchableOpacity,SafeAreaView, ActivityIndicator, ScrollView } from 'react-native'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { 
    TopNavigation,
    Divider, 
    Layout,
    Text
} from '@ui-kitten/components';
import mainStyles from '../../services/style';
import MainService from '@services/main';
import * as _ from 'lodash';
import styles from './menu.style';
import GDriveService from '../google-drive/gogole-drive.service';
import DropboxService from '../dropbox/dropbox.service';
import SnackBar from 'react-native-snackbar-component';
const color = require('@assets/custom/custom-maping.json');

class Menucomponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showAlert: false,
            message: '',
            alertStatus: 'success',
            disableMenuDocument: false
        };
    }

    render() {
    return (
        <SafeAreaView style={mainStyles.container}>
            <TopNavigation    
                title={()=><Text category='s1'>Add Video or Audio</Text>}  
                alignment='center'
            />
            <Divider/>
            <Layout style={styles.wrapMenu}>
                <ScrollView>
                    <Layout style={{padding:15}}>
                        {/* ========== START DOWNLOAD MENU ========== */}
                        <TouchableOpacity 
                            style={[styles.menuItem,{borderColor:color['color-primary-500']}]}
                            onPress={()=> this.props.navigation.navigate('Download')}
                        >
                            <FontAwesome5 color={color['color-primary-400']} name='cloud-download-alt' size={18} style={{marginRight: 15}}/>
                            <Text style={styles.menuText}>Download</Text>
                        </TouchableOpacity>
                        {/* ========== END DOWNLOAD MENU ========== */}

                        {/* ========== START GOOGLE DRIVE MENU ========== */}
                        <TouchableOpacity 
                            style={[styles.menuItem,{borderColor:color['color-primary-500']}]}
                            onPress={()=> {
                                GDriveService.googleSign().then(()=>{
                                    this.props.navigation.navigate('GoogleDrive');
                                }).catch(err =>{
                                    console.log(err);
                                });
                            }}
                        >
                            <FontAwesome5 color={color['color-primary-400']} name='google-drive' size={18} style={{marginRight: 15}}/>
                            <Text style={styles.menuText}>Google Drive</Text>
                        </TouchableOpacity>
                        {/* ========== END GOOGLE DRIVE MENU ========== */}

                        {/* ========== START DROPBOX MENU ========== */}
                        <TouchableOpacity 
                            style={[styles.menuItem,{borderColor:color['color-primary-500']}]}
                            onPress={()=> {
                                DropboxService.dropboxSign().then(()=>{
                                    this.props.navigation.navigate('Dropbox');
                                }).catch(err =>{
                                    console.log(err);
                                });
                            }}
                        >
                            <FontAwesome5 color={color['color-primary-400']} name='dropbox' size={18} style={{marginRight: 15}}/>
                            <Text style={styles.menuText}>Dropbox</Text>
                        </TouchableOpacity>
                        {/* ========== END DROPBOX MENU ========== */}

                        {/* ========== START WIFI MENU ========== */}
                        <TouchableOpacity 
                            style={[styles.menuItem,{borderColor:color['color-primary-500']}]}
                            onPress={()=> {
                                this.props.navigation.navigate('Wifi');
                            }}
                        >
                            <FontAwesome5 color={color['color-primary-400']} name='wifi' size={18} style={{marginRight: 15}}/>
                            <Text style={styles.menuText}>Transfer Wifi</Text>
                        </TouchableOpacity>
                        {/* ========== END WIFI MENU ========== */}
                    </Layout>
                </ScrollView>
            </Layout>

            <SnackBar 
                visible={this.state.showAlert} 
                messageColor="#FFFFFF"
                accentColor="#FFFFFF"
                textMessage={this.state.message}
                backgroundColor={color['color-'+ this.state.alertStatus +'-500']}
                actionHandler={()=>this.setState({showAlert: false})} 
                actionText="OK"
            />
        </SafeAreaView>
    )
  }
}
export default Menucomponent;