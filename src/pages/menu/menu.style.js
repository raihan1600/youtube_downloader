import {StyleSheet, Dimensions} from 'react-native';

const styles = StyleSheet.create({
    wrapMenu:{
    flex: 1,
		backgroundColor:'#ffffff'
    },
    wrapMenuItem:{
        paddingHorizontal:15,
    },
    wrapMenuBg:{
		backgroundColor:'#ffffff',
    },
    menuItem:{
		borderWidth: 1,
		padding: 20,
		borderRadius: 5,
		marginBottom: 15,
		flexDirection: 'row'
    },
	menuText: {
		fontSize:18
	}
});

export default styles;
