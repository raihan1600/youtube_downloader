import React, { Component } from 'react';
import { 
    TopNavigation,
    Divider, 
    Layout,
    Text
} from '@ui-kitten/components';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import mainStyles from '@services/style';
import { ScrollView, SafeAreaView } from 'react-native';
const color = require('@assets/custom/custom-maping.json');

class BrowseGalleryComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <SafeAreaView>
                 <TopNavigation    
                    title={()=><Text category='s1'>Browse Gallery</Text>}  
                    alignment='center'
                    accessoryLeft={()=> (
						<MaterialIcons 
							color='#3A3A3A' 
							name='arrow-back' 
							size={28}
							onPress={()=> this.props.navigation.goBack()}
							style={mainStyles.iconTopNavigation}
						/>
					)}
                />
                <Layout style={{flex:1}}>
                    <ScrollView>
                        <Text> browse-gallery.component </Text>
                    </ScrollView>
                </Layout>
            </SafeAreaView>
        );
    }
}

export default BrowseGalleryComponent;
