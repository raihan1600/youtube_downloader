import React, { Component } from 'react'
import { 
    TopNavigation ,
    Text, 
    Divider,
    Layout,
    Button,
    TabBar, 
    Tab
} from '@ui-kitten/components';
import { Dimensions, Image, ScrollView, Animated, View, TouchableOpacity, Alert } from 'react-native';
import mainStyles from '@services/style';
import { connect } from 'react-redux';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { RNFFmpeg } from 'react-native-ffmpeg';
import MainService from '@services/main';
import Video from 'react-native-video';
import { store } from '@services/store';
import {launchImageLibrary} from 'react-native-image-picker';
import { GestureHandlerRootView, PanGestureHandler  } from 'react-native-gesture-handler';
import videoEditorService from './video-editor.service';
import styles from './video-editor.style'
import * as _ from 'lodash';
const color = require('@assets/custom/custom-maping.json');
const RNFS = require('react-native-fs');
const windowWidth = Dimensions.get('window').width;
const editorPath = `${RNFS.DocumentDirectoryPath}/Video-Editor`;
const framePath = `${RNFS.DocumentDirectoryPath}/Frames`;
const point = 5;
const pointSize = 52;
const split = 10;
let translateXPoint = 0;

class VideoEditorComponent extends Component {
    constructor(props) {
		super(props);
		this.state = {
            videoUri: '',
            videoPaused: true,
            fileName: '',
            frameMinMargin: 0,
            duration: 120,
            listFrame: [],
            tabIndex: 0,
            timelineWidth: 0,
            animateDuration: new Animated.Value(0),
            isEnded: false,
            arrSplitPosition: [],
            splitDuration: '0',
            splitLinePosition: 0,
            splitButtonLoading: false
        };
	}

    async componentDidMount(){
        this.setState({
            videoUri: this.props.videoEditor.videoUri,
            fileName: this.props.videoEditor.fileName,
            duration: this.props.videoEditor.duration,
            listFrame: this.props.videoEditor.listFrame,
            timelineWidth: this.props.videoEditor.timelineWidth,
            frameMinMargin: this.props.videoEditor.frameMinMargin,

            arrSplitPosition: this.props.videoEditor.arrSplitPosition,
            splitDuration: this.props.videoEditor.splitDuration,
            splitLinePosition: this.props.videoEditor.splitLinePosition
        });
    }

    setAnimateDuration = (n) => {
        Animated.timing(this.state.animateDuration, {
            toValue: n,
            duration: 0,
            useNativeDriver: true
        }).start();
    }

    // ============================= //
    // OPEN VIDEO
    // ============================= //
    async openVideo(){
        const result = await launchImageLibrary({mediaType: 'video'});
        if(result['assets']){
            this.setState({
                listFrame: [],
                duration: 0,
                frameMinMargin: 0,
                arrSplitPosition: [],
                splitDuration: 0,
                splitLinePosition: 0
            });
            let data = result['assets'][0];
            let duration = Math.round(data['duration']);
            let fileName = data['fileName'];
            this.setState({
                videoUri: data['uri'],
                fileName: fileName,
                duration: duration,
                timelineWidth: duration*7
            },()=>{
                this.state.animateDuration.setValue(0);
                videoEditorService.getFFMPEGLog(duration);
            });
    
            RNFFmpeg.cancel();
            await RNFS.unlink(framePath);
            await RNFS.mkdir(framePath);
            let arrIndex = [];

            for(let i=0; i< duration; i++){
                if(i % point === 0){
                    arrIndex.push(i);
                }
            }

            let frameWidth = arrIndex.length * pointSize;
            let gap = frameWidth - this.state.timelineWidth;
            let calc = (gap/arrIndex.length) * -1;
            this.setState({frameMinMargin: calc},()=> {
                console.log(fileName,calc,gap,arrIndex.length,this.state.timelineWidth)
            });

            for(let i=0; i< arrIndex.length; i++){
                let resPath = await videoEditorService.createFramesPersecond(data['uri'],arrIndex[i]);
                this.state.listFrame.push(resPath);
                this.setState({listFrame: this.state.listFrame});
            }

            store.dispatch({ 
                type: "UPDATE_VIDEO_EDITOR_INFO", 
                videoUri: data['uri'],
                fileName: data['fileName'],
                duration: duration,
                listFrame: this.state.listFrame,
                timelineWidth: this.state.timelineWidth,
                frameMinMargin: calc,
            });	

            store.dispatch({ 
                type: "UPDATE_VIDEO_EDITOR_SPLIT", 
                splitDuration: 0,
                splitLinePosition: 0
            });	
        }
    }

    // ============================ //
    // HANDLE GESTURE
    // ============================ //
    handleGesture = (evt) =>{
        if(!this.state.videoPaused) this.setState({videoPaused: true});
        let{nativeEvent} = evt;
        let targetPosition = translateXPoint + nativeEvent.translationX;
        if(targetPosition <= this.state.timelineWidth && targetPosition >= 0){
            this.updatePosition(targetPosition);
            this.setAnimateDuration(targetPosition);
        }
    }

    // ============================ //
    // UPDATE POSITION
    // ============================ //
    updatePosition = _.debounce((n)=>{
        translateXPoint = n;
        
        let percentage = n / this.state.timelineWidth;
        let durationPosition = this.state.duration * percentage;

        this.setState({
            splitLinePosition: n,
            splitDuration: Math.round(durationPosition)
        });

        store.dispatch({ 
            type: "UPDATE_VIDEO_EDITOR_SPLIT", 
            splitDuration: Math.round(durationPosition),
            splitLinePosition: n
        });	
        
        if(this.state.videoUri){
            if(this.player) this.player.seek(durationPosition);
        }
    }, 500);

    // ============================ //
    // SPLIT VIDEO
    // ============================ //
    splitVideo = () =>{
        let percentage = this.state.splitLinePosition / this.state.timelineWidth;
        let durationPosition = this.state.duration * percentage;
        let roundDuration = Math.round(durationPosition);
        if(roundDuration >= 5){
            let splitTime = this.state.duration / roundDuration;
            let arrSplit = [];
            for(let i=1; i<splitTime; i++){
                arrSplit.push(i * roundDuration);
            }
            arrSplit.push(this.state.duration);
    
            let arrPosition = [];
            arrSplit.map((x,i)=>{
                let setPosition = this.state.timelineWidth * (x/this.state.duration);
                let lastChunk = i == 0 ? '00:00:00' : arrPosition[arrPosition.length - 1]['endTime'];
                arrPosition.push({
                    x: setPosition,
                    startTime: lastChunk,
                    endTime: MainService.convertSeconds(x)
                });
            });
            this.setState({arrSplitPosition:arrPosition});
            store.dispatch({ 
                type: "UPDATE_VIDEO_EDITOR_SPLIT_ARR", 
                arrSplitPosition: arrPosition
            });	
        }else{
            Alert.alert('Warning','Split video minimum 5 second', [
                {
                    text: "OK",
                    onPress: () => null,
                }
            ]);
        }
    }

    render() {
        // ============================ //
        // ANIMATED TRANSLATE-X
        // ============================ //
        const animatePlayDuration = {
            transform: [
                { translateX: this.state.animateDuration }
            ]
        }

        // ============================ //
        // CREATE FRAME
        // ============================ //
        var arrTimeline = [];
        for(let i = 0; i < this.state.duration; i++){
            if(i % 5 === 0 && i != 0){
                if(i % 10 === 0 && i != 0){
                    arrTimeline.push(
                        <Layout key={i} style={styles.numberTimeline}>
                            <Text style={styles.textTimeline} category='s1'>{MainService.convertSeconds(i)}</Text>
                            <Layout style={[styles.point,styles.pointBig,{marginTop:17}]}></Layout>
                        </Layout>
                    )
                }else{
                    arrTimeline.push(<Layout key={i} style={[styles.point,styles.pointBig,styles.pointSingleBig]}></Layout>)
                }
            }else{
                arrTimeline.push(<Layout key={i} style={[styles.point,styles.pointSingle]}></Layout>)   
            }
        }

        return (
            <>
                {/* ============== START TOP NAVIGATOR ============== */}
                <TopNavigation 
                    title={()=> <Text category='s1'>Video Editor</Text>}
                    subtitle={()=> <Text category='p2' numberOfLines={1} style={{width:'50%',color:'#999'}}>{this.state.fileName}</Text>}
                    alignment='center' 
                    accessoryLeft={()=> {
                        if(this.state.videoUri){
                            return (
                                <Feather 
                                    color='#3A3A3A' 
                                    name='search' 
                                    size={25}
                                    style={mainStyles.iconTopNavigation}
                                    onPress={()=>this.openVideo()}
                                />
                            )
                        }else{
                            return null
                        }
                    }}
                    accessoryRight={()=> (
                        <Feather 
                            color='#3A3A3A' 
                            name='folder' 
                            size={25}
                            style={mainStyles.iconTopNavigation}
                            onPress={()=>this.openVideo()}
                        />
                    )}
                />
                {/* ============== END TOP NAVIGATOR ============== */}

                <Divider />

                {/* ============== START VIDEO DISPLAY ============== */}
                {this.state.videoUri ? 
                    <Layout style={{position:'relative', zIndex:5}}>
                        <Video 
                            source={{uri: this.state.videoUri}}   
                            ref={(ref) => {
                                this.player = ref
                            }}             
                            resizeMode="contain"
                            onProgress={(e)=> {
                                let percentage  = e.currentTime / e.playableDuration;
                                if(!this.state.videoPaused){
                                    this.setAnimateDuration(this.state.timelineWidth * percentage);
                                    this.updatePosition(this.state.timelineWidth * percentage);
                                }
                                this.setState({isEnded: false});
                            }}
                            onEnd={()=> {
                                this.setState({
                                    videoPaused: true,
                                    isEnded: true
                                });
                            }}
                            paused={this.state.videoPaused}                         
                            style={{
                                backgroundColor: '#000000',
                                width: windowWidth,
                                height: windowWidth/2
                            }} 
                        />
                        <TouchableOpacity style={styles.playBtn} onPress={()=>{
                            if(this.state.isEnded){
                                this.player.seek(0);
                            }

                            this.setState({videoPaused: !this.state.videoPaused});
                        }}>
                            <Image 
                                style={{
                                    width: 30,
                                    height: 30,
                                    borderWidth: 2,
                                    borderColor: '#fff',
                                    borderRadius: 15
                                }}
                                source={!this.state.videoPaused ? require("@assets/images/pause.png") : require("@assets/images/buttonplay.png")}
                            />
                        </TouchableOpacity>
                    </Layout>
                : 
                    <TouchableOpacity 
                        onPress={()=>this.openVideo()}
                        style={styles.wrapAddVideo}>
                            <Layout 
                                style={styles.overlayAddVideo}
                            />
                            <Feather color='#9A9A9A' name="video" size={35} />
                            <Text category="p2" style={{color:'#9A9A9A',marginTop:10}}>Click here to browse video</Text>
                    </TouchableOpacity>
                }
                 {/* ============== END VIDEO DISPLAY ============== */}

                <Divider />

                <TabBar
                    selectedIndex={this.state.tabIndex}
                    indicatorStyle={{display:'none'}}
                    onSelect={index => this.setState({tabIndex: index})}>
                    <Tab 
                        title={()=><Text category="c2" style={{marginTop:3,color:this.state.tabIndex === 0 ? color['color-primary-500'] : '#333'}}>Split</Text>} 
                        style={{paddingVertical: 5}} 
                        icon={()=> <Feather name='columns' color={this.state.tabIndex === 0 ? color['color-primary-500'] : '#333'} size={18}/>} 
                    />
                    <Tab 
                        title={()=><Text category="c2" style={{marginTop:3}}>Trim</Text>} 
                        style={{paddingVertical: 5}} 
                        icon={()=> <Feather name='scissors' size={18}/>} 
                    />
                    <Tab 
                        title={()=><Text category="c2" style={{marginTop:3}}>Speed</Text>} 
                        style={{paddingVertical: 5}} 
                        icon={()=> <Feather name='clock' size={18}/>} 
                    />
                    <Tab 
                        title={()=><Text category="c2" style={{marginTop:3}}>Adjust</Text>} 
                        style={{paddingVertical: 5}} 
                        icon={()=> <Feather name='sliders' size={18}/>} 
                    />
                    <Tab 
                        title={()=><Text category="c2" style={{marginTop:3}}>Crop</Text>} 
                        style={{paddingVertical: 5}} 
                        icon={()=> <Feather name='crop' size={18}/>} 
                    />
                    <Tab 
                        title={()=><Text category="c2" style={{marginTop:3}}>Filter</Text>} 
                        style={{paddingVertical: 5}} 
                        icon={()=> <Feather name='layers' size={18}/>} 
                    />
                </TabBar>
                
                <Divider />
                <Layout style={{flex:1}}>
                    <ScrollView 
                        horizontal={true}
                        alwaysBounceHorizontal={false}
                        bounces={false}
                        ref={ref => (this.ScrollView = ref)}
                    >
                        <Layout style={{flex:1, position:'relative'}}>
                            <GestureHandlerRootView style={{position:'relative', zIndex: 9}}>
                                <PanGestureHandler onGestureEvent={this.handleGesture}>
                                    <Animated.View style={[styles.redCircle,animatePlayDuration]}>
                                        <Feather color='#FFFFFF' name='more-horizontal' size={19} style={{marginLeft:1}}/>
                                    </Animated.View>
                                </PanGestureHandler>
                            </GestureHandlerRootView>

                            <Divider />
                            <Layout style={{flex:1, position: 'relative'}}>
                                <Layout style={styles.wrapTimeline}>
                                    <Layout style={{flexDirection:'row'}}>{arrTimeline}</Layout>
                                </Layout>

                                <Layout style={{
                                    paddingVertical: 10,
                                    paddingHorizontal: 20,
                                }}>
                                    <Layout style={{
                                        flexDirection: 'row',
                                        borderRadius: 10,
                                        marginBottom: 10,
                                        marginTop: 5,
                                    }}>

                                        {this.state.listFrame.map((x,i)=>(
                                            <Image 
                                                key={i}
                                                resizeMode="contain"
                                                source={{uri: `file://${x}`}} 
                                                style={{
                                                    width: pointSize,
                                                    height: pointSize,
                                                    marginLeft: i != 0 ? this.state.frameMinMargin : 0,
                                                    marginRight: i == 0 ? this.state.frameMinMargin : 0,
                                                    backgroundColor:'#000'
                                                }}
                                            />
                                        ))}
                                    </Layout>
                                </Layout>
                            </Layout>

                            {this.state.arrSplitPosition.map((item,i)=>(
                                <View key={i} style={[
                                    styles.redLine,
                                    {
                                        marginLeft: item.x + 1, 
                                        top: 70,
                                        backgroundColor: '#fff'
                                    }
                                ]} />
                            ))}  

                            <Animated.View style={[styles.redLine,animatePlayDuration]} />
                        </Layout>
                    </ScrollView>
                </Layout>

                <Divider />

                <Text style={{padding:5,textAlign:'center'}} category="c2">
                    Split per {MainService.convertSeconds(this.state.splitDuration)}
                </Text>
                
                <Divider />

                <Layout style={styles.wrapAction}>
                    <Layout style={styles.itemAction}>
                        <Button 
                            disabled={!this.state.videoUri}
                            status="basic"
                            style={{borderRadius: 5}}
                            onPress={()=>this.splitVideo()}
                            accessoryRight={()=>(<MaterialIcons color='#333333' name='check' size={20}/>)}
                        >
                            Set Split
                        </Button>
                    </Layout>
                    <Layout style={styles.itemAction}>
                        <Button 
                            disabled={!this.state.videoUri || this.state.splitButtonLoading}
                            style={{borderRadius: 5}}
                            onPress={async ()=>{
                                this.setState({splitButtonLoading: true});
                                await RNFS.unlink(editorPath);
                                await RNFS.mkdir(editorPath);
                                videoEditorService.getFFMPEGLog(this.state.duration);
                                for(let i=0; i< this.state.arrSplitPosition.length; i++){
                                    let x = this.state.arrSplitPosition[i];
                                    let cut = await videoEditorService.cutVideo(this.state.videoUri,x.startTime,x.endTime,`${i}_${this.state.fileName}`);
                                    console.log(cut);                                    
                                }
                                this.setState({splitButtonLoading: false});
                            }}
                            accessoryRight={()=> (<Feather color={this.state.videoUri && !this.state.splitButtonLoading ? '#FFFFFF' : '#999999'} name={this.state.splitButtonLoading ? 'clock' : 'play'} size={20}/>)}
                        >
                            {this.state.splitButtonLoading ? "Extracting..." : "Execute Split"}
                        </Button>
                    </Layout>
                </Layout>
            </>
        )
    }
}
function mapStateToProps(state) {
    return {
        videoEditor: state.videoEditor
    }
}
export default connect(mapStateToProps, null, null, { forwardRef: true })(VideoEditorComponent);