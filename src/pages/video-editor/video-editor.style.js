import {StyleSheet, Dimensions} from 'react-native';
const color = require('@assets/custom/custom-maping.json');
const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    // ==================================== //
    // TIMELINE
    // ==================================== //
    wrapTimeline: {
        flexDirection: 'row',
        height: 43,
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderBottomWidth: 1,
        borderColor: '#EFEFEF'
    },
    point:{
        borderRadius:1.25,
        width:2.5,
        height:2.5,
        backgroundColor: '#CFCFCF',
        marginRight: 4
    },
    pointSingle:{
        marginTop: 18,
        position:'relative',
        zIndex: 2,
    },
    pointSingleBig:{
        marginTop: 17,
        position: 'relative',
        zIndex: 1,
    },
    pointBig:{
        borderRadius:2.5,
        width:5,
        height:5,
    },
    textTimeline:{
        color: '#6a6a6a',
        fontSize: 12,
        textAlign: 'center',
        marginBottom: 5,
        position: 'absolute',
        top: -3,
        left: -28,
        width: 60
    },
    numberTimeline:{
        alignItems: 'center',
        position: 'relative'
    },
    redLine:{
        position:'absolute',
        top: 0,
        left: 20,
        width: 2,
        height: '100%',
        backgroundColor: color['color-danger-500'],
        zIndex: 5
    },
    redCircle:{
        left:10,
        width: 22,
        height: 20,
        backgroundColor: color['color-danger-500'],
        zIndex: 5,
        borderRadius: 2
    },
    blueLine:{
        position:'absolute',
        top: 0,
        left: 10,
        width: 2,
        height: '100%',
        backgroundColor: color['color-info-500'],
        zIndex: 5
    },

    // ==================================== //
    // BUTTON on BOTTOM
    // ==================================== //
    wrapAction:{
        padding:5, 
        flexDirection:'row',
    },
    itemAction:{
        flex:1,
        paddingHorizontal:3,
    },
    itemButton:{
        paddingVertical: 0
    },

    // ==================================== //
    // ADD VIDEO
    // ==================================== //
    wrapAddVideo: {
        justifyContent: 'center',
        alignItems:'center',
        width: windowWidth,
        height: windowWidth/2,
        backgroundColor: '#F5F5F5'
    },
    overlayAddVideo: {
        width: '80%',
        height: '80%',
        borderWidth: 2,
        borderColor: '#DFDFDF',
        position: 'absolute',
        left: '10%',
        top: '10%',
        borderRadius: 10,
        borderStyle: 'dashed'
    },
    playBtn:{
        width: 30,
        height: 30,
        position: 'absolute',
        top: 5,
        left: 5,
    }
});

export default styles;
