export const videoEditorReducer = (state = {
	videoUri: '',
    fileName: '',
    duration: '',
    listFrame: [],
    timelineWidth: 0,
    frameMinMargin: 0,
    arrSplitPosition: [],
    splitDuration: '0',
    splitLinePosition: 0
}, action) => {
	switch (action.type) {
		case "UPDATE_VIDEO_EDITOR_INFO":
			state = {
				...state,
                videoUri: action.videoUri,
                fileName: action.fileName,
                duration: action.duration,
                listFrame: action.listFrame,
                timelineWidth: action.timelineWidth,
                frameMinMargin: action.frameMinMargin
			}
        break;
        case "UPDATE_VIDEO_EDITOR_SPLIT":
			state = {
				...state,
                splitDuration: action.splitDuration,
                splitLinePosition: action.splitLinePosition
			}
        break;
        case "UPDATE_VIDEO_EDITOR_SPLIT_ARR":
			state = {
				...state,
                arrSplitPosition: action.arrSplitPosition
			}
        break;
	}
	return state;
}