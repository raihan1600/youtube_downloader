import { RNFFmpeg,RNFFprobe,RNFFmpegConfig } from 'react-native-ffmpeg';
import * as _ from 'lodash';
const RNFS = require('react-native-fs');

const videoEditorService = {
    // ========================= //
    // Time to String
    // ========================= //
    timeString2ms: function(time){
        var arr = time.split(':');
        var hh = arr[0] * 60 * 60;
        var mm = arr[1] * 60;
        var ss = arr[2];
        return Number(hh) + Number(mm) + Math.trunc(Number(ss));
    },
    
    // ================================= //
    // FFMPEG PROCESS
    // ================================= //
    getFFMPEGLog: (totalDuration)=>{
        RNFFmpegConfig.enableLogCallback((e)=>{
            let getTime = e.message.match(/time=(.*) bitrate/) === null ? e.message.match(/time= (.*) bitrate/) : e.message.match(/time=(.*) bitrate/);
            if(getTime !== null){
                let current = videoEditorService.timeString2ms(getTime[1]);
                let setpercentage = Math.ceil((current / totalDuration) * 100);
                // console.log(setpercentage);
            }
        });
    },

    splitVideo: (input,time)=>{
        return new Promise(async (resolve,reject)=>{
            let destPath = RNFS.DocumentDirectoryPath + '/Video-Editor';
            RNFFmpeg.execute(`-i ${input} -c copy -map 0 -segment_time ${time} -f segment -reset_timestamps 1 ${destPath}/output%03d.mp4`).then(result =>{
                resolve({
                    path: destPath,
                    result: result
                });
            });
        });
    },

    createWaveform: (input,width,height)=> {
		return new Promise(async (resolve,reject)=>{
            let destPath = 'Video-Editor/output_waveform.png';
            let exist = await RNFS.exists(`${RNFS.DocumentDirectoryPath}/${destPath}`);
            let size = `${width}x${height}`;
            if(exist){
                await RNFS.unlink(`${RNFS.DocumentDirectoryPath}/${destPath}`);
            }
            RNFFmpeg.execute(`-i ${input} -filter_complex "gradients=s=${size}:c0=553AFA:c1=553AFA:x0=0:x1=0:y0=0:y1=1080,drawbox=x=(iw-w)/2:y=(ih-h)/2:w=iw:h=1:color=#4836CB[bg];[0:a]aformat=channel_layouts=mono,showwavespic=s=${size}:colors=#4836CB[fg];[bg][fg]overlay=format=auto" -vframes:v 1 ${RNFS.DocumentDirectoryPath}/${destPath}`).then(result =>{
				resolve(`${RNFS.DocumentDirectoryPath}/${destPath}`);
			});
        });
	},
    cutVideo: (input,start_time,end_time,name)=> {
		return new Promise(async (resolve,reject)=>{
            let output = `${RNFS.DocumentDirectoryPath}/Video-Editor/${name}`;
            RNFFmpeg.execute(`-ss ${start_time} -to ${end_time} -i ${input} -c copy ${output}`).then(result =>{
                resolve(result);
            });
        });
	},
    createFramesPersecond: (input,index)=> {
		return new Promise(async (resolve,reject)=>{
            let pathFrame = `${RNFS.DocumentDirectoryPath}/Frames`;
            RNFFmpeg.execute(`-ss ${videoEditorService.convertSeconds(index)} -i ${input} -frames:v 1 -q:v 2 -vf scale=50:-1 ${pathFrame}/output_${index}.jpg`).then(()=>{
                resolve(`${pathFrame}/output_${index}.jpg`);
            }).catch(()=>{
                reject("Error");
            })
        });
	},
    createFrames: (input,duration,splitNumber)=> {
		return new Promise(async (resolve,reject)=>{
            let pathFrame = `${RNFS.DocumentDirectoryPath}/Frames`;
            await RNFS.unlink(pathFrame);
            await RNFS.mkdir(pathFrame);
            let point = Math.round(duration/splitNumber);

            let pad = (n)=>{
                if(n < 10){
                    return "0" + n;
                }
                return n;
            };

            let arrFiles = [];
            let arrIndex = [];
            for(let i=0; i<duration; i++){
                if(i % point === 0){
                    await RNFFmpeg.execute(`-ss ${videoEditorService.convertSeconds(i)} -i ${input} -frames:v 1 -q:v 2 ${pathFrame}/output_${pad(i)}.jpg`);
                    arrFiles.push(`${pathFrame}/output_${pad(i)}.jpg`);
                    arrIndex.push(arrFiles.length - 1);
                }
            }
            let joinArr = arrFiles.join(' -i ');
            let joinArrIndex = arrIndex.join('][');
            await RNFFmpeg.execute(`-i ${joinArr} -filter_complex "[${joinArrIndex}]hstack=inputs=${arrFiles.length}" ${pathFrame}/image_frame.jpg`);
            arrFiles.map(async (x)=>{
                await RNFS.unlink(x);
                resolve(`${pathFrame}/image_frame.jpg`);
            });
        });
	},
    convertSeconds(seconds) {
        var convert = function(x) { return (x < 10) ? "0"+x : x; }
        return convert(parseInt(seconds / (60*60))) + ":" +
                convert(parseInt(seconds / 60 % 60)) + ":" +
                convert(seconds % 60)
	},
    getInformation: (url) =>{
        return new Promise((resolve,reject)=>{
            RNFFprobe.getMediaInformation(url).then(information => {
                resolve(information);
            })
        });
    },
};

export default videoEditorService;