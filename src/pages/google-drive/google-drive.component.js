import React, { Component } from 'react';
import { 
	Divider, 
    ListItem,
	Layout,
	Text, 
	TopNavigation,
    Button
} from '@ui-kitten/components';
import { RefreshControl, SafeAreaView, ScrollView } from 'react-native';
import Feather from "react-native-vector-icons/Feather";
import { store } from '@services/store';
import { connect } from 'react-redux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import mainStyles from '@services/style';
import GDriveService from './gogole-drive.service';

class GoogleDriveComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loadingButton: false,
            refreshing: false
        };
    }

    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
				<TopNavigation 
					title={()=> <Text category='s1'>Google Drive</Text>}
                    subtitle={()=> <Text>{this.props.gdrive.folderName}</Text>}
					alignment='center'
					accessoryLeft={()=> (
						<MaterialIcons 
							color='#3A3A3A' 
							name='arrow-back' 
							size={28}
							onPress={async ()=> {
                                if(this.props.gdrive.folderId){
                                    GDriveService.toRoot(this.props.gdrive.googleToken).then((files)=> {
                                        store.dispatch({ type: "UPDATE_GOOGLE_DRIVE_FOLDER", folderId: null, folderName: null });
                                        store.dispatch({ type: "UPDATE_GOOGLE_DRIVE_LIST", googleDriveList: files });
                                    }).catch(err =>{
                                        store.dispatch({ type: "UPDATE_GOOGLE_DRIVE_LIST", googleDriveList: [] });
                                    });
                                }else{
                                    this.props.navigation.goBack()
                                }
                            }}
							style={mainStyles.iconTopNavigation}
						/>
					)}
				/>
				<Divider />
                <ScrollView
                    style={{backgroundColor:'#ffffff'}}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={async ()=>{
                                this.setState({refreshing: true});
                                let files = [];
                                if(this.props.gdrive.folderId){
                                    files = await  GDriveService.intoFolder(this.props.gdrive.googleToken,this.props.gdrive.folderId);
                                }else{
                                    files = await GDriveService.toRoot(this.props.gdrive.googleToken);
                                }
                                store.dispatch({ type: "UPDATE_GOOGLE_DRIVE_LIST", googleDriveList: files });
                                this.setState({refreshing: false});
                            }}
                        />
                    }
                >
                    <Layout style={{flex:1}}>
                        {this.props.gdrive.googleDriveList.map((x,i)=>(
                            <Layout key={i}>
                            <ListItem
                                underlayColor='none'
                                title={()=> <Text category='h6' style={mainStyles.textList}>{x.name}</Text> }
                                accessoryLeft={()=>(
                                    <Layout style={mainStyles.wrapFileicon}>
                                        {x.mimeType == "application/vnd.google-apps.folder" ? <Feather name='folder' size={20} color="#3A3A3A"/> : null}
                                        {x.fileExtension == "mp3" ? <Feather name='music' size={20} color="#3A3A3A"/> : null}
                                        {x.fileExtension == "mp4" ? <Feather name='film' size={20} color="#3A3A3A"/> : null}
                                    </Layout>
                                )}  
                                onPress={()=>{
                                    if(x.mimeType == "application/vnd.google-apps.folder"){
                                        GDriveService.intoFolder(this.props.gdrive.googleToken,x.id).then((files)=> {
                                            store.dispatch({ type: "UPDATE_GOOGLE_DRIVE_FOLDER", folderId: x.id, folderName: x.name });
                                            store.dispatch({ type: "UPDATE_GOOGLE_DRIVE_LIST", googleDriveList: files });
                                        }).catch(err =>{
                                            store.dispatch({ type: "UPDATE_GOOGLE_DRIVE_LIST", googleDriveList: [] });
                                        });
                                    }else{
                                        this.props.navigation.navigate('Download',{
                                            urlDownload: x.webContentLink, 
                                            downloadType: x.fileExtension == "mp3" ? 'Audios' : 'Videos',
                                            extension: x.fileExtension == "mp3" ? 'mp3' : 'mp4'
                                        });
                                    }
                                }}
                            />
                            <Divider />
                            </Layout>
                        ))}
                    </Layout>
                </ScrollView>
                {this.props.gdrive.googleNextToken ? 
                    <>
                        <Divider />
                        <Layout style={{padding:15}}>
                            <Button 
                                disabled={this.state.loadingButton}
                                status='basic'
                                onPress={()=>{
                                    this.setState({loadingButton: true});
                                    if(this.props.gdrive.folderId){
                                        GDriveService.intoFolder(
                                            this.props.gdrive.googleToken,
                                            this.props.gdrive.folderId,
                                            this.props.gdrive.googleNextToken
                                        ).then((files)=> {
                                            files.map((x)=> this.props.gdrive.googleDriveList.push(x))
                                            store.dispatch({ type: "UPDATE_GOOGLE_DRIVE_LIST", googleDriveList: this.props.gdrive.googleDriveList });
                                            this.setState({loadingButton: false});
                                        });
                                    }else{
                                        GDriveService.toRoot(
                                            this.props.gdrive.googleToken,
                                            this.props.gdrive.googleNextToken
                                        ).then((files)=> {
                                            files.map((x)=> this.props.gdrive.googleDriveList.push(x))
                                            store.dispatch({ type: "UPDATE_GOOGLE_DRIVE_LIST", googleDriveList: this.props.gdrive.googleDriveList });
                                            this.setState({loadingButton: false});
                                        });
                                    }
                                }}>
                                {this.state.loadingButton ? "Loading..." : "Load more"}
                            </Button>
                        </Layout>   
                    </>
                : null}
                <Divider />
            </SafeAreaView>
        );
    }
}

function mapStateToProps(state) {
    return {
        init: state.init,
        gdrive: state.googleDrive
    }
}
export default connect(mapStateToProps, null, null, { forwardRef: true })(GoogleDriveComponent);
