import { InAppBrowser } from 'react-native-inappbrowser-reborn';
import axios from 'axios';
import * as _ from 'lodash';
import { store } from '@services/store';
import { getDeepLink } from '@services/utilities';
const CLIENT_ID = "606222081528-45fp651c4kj2393j0tl4d38qiscdr4it.apps.googleusercontent.com";
const CLIENT_SCREET = "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";
const GOOGLE_AUTHORIZATION_URL = 'https://accounts.google.com/o/oauth2/v2/auth';
const GOOGLE_TOKEN_URL = 'https://www.googleapis.com/oauth2/v4/token';
const URL_DIRECT =  "https://medova.anurdin.net/success-login";
const qs = require('qs');
let accessToken = null;
let nextPageToken = null;
let pageSize = 20;

const GDriveService = {
    googleSign: () => {
        return new Promise(async (resolve, reject) => {
            const deepLink = getDeepLink();
            const urlParams = {
                response_type: 'code',
                redirect_uri: URL_DIRECT,
                client_id: CLIENT_ID,
                scope: 'https://www.googleapis.com/auth/drive',
                prompt: 'select_account',
            }
            const urlGoogleSign = `${GOOGLE_AUTHORIZATION_URL}?${qs.stringify(urlParams)}`;
           
            if (await InAppBrowser.isAvailable()) {
                InAppBrowser.openAuth(urlGoogleSign, deepLink, {
                    ephemeralWebSession: false,
                    showTitle: false,
                    enableUrlBarHiding: true,
                    enableDefaultShare: false
                }).then(async (response) => {
                    let query = response.url.split("?")[1];
                    let data = qs.parse(query);
                    if(data.code){
                        resolve();
                    }else{
                        reject();
                    }
                    let userInfo = await GDriveService.getGoogleToken(data.code);
                    accessToken = userInfo.access_token;
                    let files = await GDriveService.toRoot(accessToken);
                    store.dispatch({ type: "UPDATE_GOOGLE_DRIVE_TOKEN", googleToken: accessToken});
                    store.dispatch({ type: "UPDATE_GOOGLE_DRIVE_LIST", googleDriveList: files });
                    
                }).catch((err) =>{
                    reject(err);
                });
            }
        }); 
    },
    getGoogleToken: (code) => {
        return new Promise((resolve, reject) => {
            axios.post(GOOGLE_TOKEN_URL, qs.stringify({
                code: code,
                client_id: CLIENT_ID,
                client_secret: CLIENT_SCREET,
                redirect_uri: URL_DIRECT,
                grant_type: 'authorization_code'
            }), {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
            }).then(res => {
                resolve(res['data']);
            }).catch(err => {
                reject(err);
            });
        });
    },
    loadMore: (token, pageToken, folderID, searchValue) => {
        return new Promise((resolve, reject) => {
            var headers = {
                Authorization: `Bearer ${token}`
            }

            var q = searchValue !== "" ? "name contains '" + searchValue + "' and (mimeType = 'image/jpeg')" : "'" + folderID + "' in parents and (mimeType = 'application/vnd.google-apps.folder' or mimeType = 'image/jpeg')";

            const urlParams = {
                q: q,
                orderBy: "folder,name",
                fields: "*",
                pageToken: pageToken
            }
            axios.get(`https://www.googleapis.com/drive/v3/files?${qs.stringify(urlParams)}`, { headers: headers }).then(r => {
                var nextPageToken = r.data.nextPageToken === undefined ? null : r.data.nextPageToken;
                console.log(r);
                resolve()
            }).catch(e => {
                console.log(e);
                reject();
            });
        })
    },
    searchFile: (token, value) => {
        return new Promise((resolve, reject) => {
            var headers = {
                Authorization: `Bearer ${token}`
            }

            const urlParams = {
                q: "name contains '" + value + "' and (mimeType = 'image/jpeg')",
                orderBy: "folder,name",
                fields: "*"
            }
            axios.get(`https://www.googleapis.com/drive/v3/files?${qs.stringify(urlParams)}`, { headers: headers }).then(r => {
                var nextPageToken = r.data.nextPageToken === undefined ? null : r.data.nextPageToken;
                console.log(r);
                resolve()
            }).catch(e => {
                console.log(e);
                reject();
            });
        })
    },
    intoFolder: (token, folderID, pageToken) => {
        return new Promise((resolve, reject) => {
            var headers = {
                Authorization: `Bearer ${token}`
            }

            const urlParams = {
                q: "'" + folderID + "' in parents and (mimeType = 'application/vnd.google-apps.folder' or fileExtension = 'mp3' or fileExtension = 'mp4')",
                orderBy: "folder,name",
                fields: "*",
                pageSize: pageSize
            }

            if(pageToken) urlParams["pageToken"] = pageToken;

            axios.get(`https://www.googleapis.com/drive/v3/files?${qs.stringify(urlParams)}`, { headers: headers }).then(r => {
                nextPageToken = r.data.nextPageToken ? r.data.nextPageToken : null;
                store.dispatch({ type: "UPDATE_GOOGLE_DRIVE_NEXT_TOKEN", googleNextToken: nextPageToken});
                resolve(r.data.files)
            }).catch(e => {
                reject(e);
            });
        });
    },
    toRoot: (token, pageToken) => {
        return new Promise((resolve, reject) => {
            var headers = {
                Authorization: `Bearer ${token}`
            }

            const urlParams = {
                q: "'root' in parents and (mimeType = 'application/vnd.google-apps.folder' or fileExtension = 'mp3' or fileExtension = 'mp4')",
                orderBy: "folder,name",
                fields: "*",
                pageSize: pageSize
            }

            if(pageToken) urlParams["pageToken"] = pageToken;

            axios.get(`https://www.googleapis.com/drive/v3/files?${qs.stringify(urlParams)}`, { headers: headers }).then(r => {
                nextPageToken = r.data.nextPageToken ? r.data.nextPageToken : null;
                store.dispatch({ type: "UPDATE_GOOGLE_DRIVE_NEXT_TOKEN", googleNextToken: nextPageToken});
                resolve(r.data.files);
            }).catch(e => {
                console.log(JSON.parse(JSON.stringify(e)));
                reject(e);
            });
        });
    },
};

export default GDriveService;