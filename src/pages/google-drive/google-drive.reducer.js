export const googleDriveReducer = (state = {
	googleDriveList: [],
    googleToken: null,
    googleNextToken: null,
	folderId: null,
	folderName: null
}, action) => {
	switch (action.type) {
		case "UPDATE_GOOGLE_DRIVE_FOLDER":
			state = {
				...state,
				folderId: action.folderId,
				folderName: action.folderName
			}
        break;
        case "UPDATE_GOOGLE_DRIVE_TOKEN":
			state = {
				...state,
				googleToken: action.googleToken
			}
        break;
		case "UPDATE_GOOGLE_DRIVE_NEXT_TOKEN":
			state = {
				...state,
				googleNextToken: action.googleNextToken
			}
        break;
		case "UPDATE_GOOGLE_DRIVE_LIST":
			state = {
				...state,
				googleDriveList: action.googleDriveList,
			}
        break;
	}
	return state;
}