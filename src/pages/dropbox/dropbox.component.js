import React, { Component } from 'react';
import { 
	Divider, 
    ListItem,
	Text, 
	TopNavigation, 
    Layout,
    Button
} from '@ui-kitten/components';
import { RefreshControl, SafeAreaView, ScrollView, ActivityIndicator } from 'react-native';
import Feather from "react-native-vector-icons/Feather";
import { store } from '@services/store';
import { connect } from 'react-redux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import mainStyles from '@services/style';
import DropboxService from './dropbox.service';
import SnackBar from 'react-native-snackbar-component';
import downloadService from "../download/download.service";
const themeColor = require('@assets/custom/custom-maping.json');

class DropBoxComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loadingButton: false,
            refreshing: false,
            showAlert: false
        };
    }

    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
				<TopNavigation 
					title={()=> <Text category='s1'>Dropbox</Text>}
                    subtitle={()=> <Text>{this.props.dropbox.folderName}</Text>}
					alignment='center'
					accessoryLeft={()=> (
						<MaterialIcons 
							color='#3A3A3A' 
							name='arrow-back' 
							size={28}
							onPress={()=> {
                                if(this.props.dropbox.folderId){
                                    DropboxService.getFiles(this.props.dropbox.dropboxToken,"").then((files)=> {
                                        store.dispatch({ type: "UPDATE_DROPBOX_FOLDER", folderId: null, folderName: null });
                                        store.dispatch({ type: "UPDATE_DROPBOX_LIST", dropboxList: files });
                                    }).catch(err =>{
                                        store.dispatch({ type: "UPDATE_DROPBOX_LIST", dropboxList: [] });
                                    });
                                }else{
                                    this.props.navigation.goBack()
                                }
                            }}
							style={mainStyles.iconTopNavigation}
						/>
					)}
				/>
				<Divider />
                <ScrollView
                    style={{backgroundColor:'#ffffff'}}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={async ()=>{
                                this.setState({refreshing: true});
                                let files = [];
                                if(this.props.dropbox.folderId){
                                    files = await  DropboxService.getFiles(this.props.dropbox.dropboxToken,this.props.dropbox.folderId);
                                }else{
                                    files = await DropboxService.getFiles(this.props.dropbox.dropboxToken,'');
                                }
                                store.dispatch({ type: "UPDATE_DROPBOX_LIST", dropboxList: files });
                                this.setState({refreshing: false});
                            }}
                        />
                    }
                >
                    <Layout style={{flex:1}}>
                        {this.props.dropbox.dropboxList?.map((x,i)=>(
                            <Layout key={i}>
                            <ListItem
                                underlayColor='none'
                                disabled={x.open}
                                title={()=> <Text category='h6' style={mainStyles.textList}>{x.name}</Text> }
                                accessoryLeft={()=>(
                                    <Layout style={mainStyles.wrapFileicon}>
                                        {x['.tag'] == "folder" ? <Feather name='folder' size={20} color="#3A3A3A"/> : null}
                                        {x['.tag'] == "file" ? <Feather name='file' size={20} color="#3A3A3A"/> : null}
                                    </Layout>
                                )}  
                                accessoryRight={()=> {
                                    if(x.open){
                                        return <ActivityIndicator style={{marginRight: 15}} size="small" />;
                                    }else{
                                        return null
                                    }
                                }}
                                onPress={()=>{
                                    x.open = true;

                                    if(x['.tag'] == "folder"){
                                        DropboxService.getFiles(this.props.dropbox.dropboxToken,x.id).then(resData =>{
                                            store.dispatch({ type: "UPDATE_DROPBOX_FOLDER", folderId: x.id, folderName: x.name });
                                            store.dispatch({ type: "UPDATE_DROPBOX_LIST", dropboxList: resData });
                                            x.open = false;
                                        }).catch(errData =>{
                                            this.setState({showAlert: true});
                                            x.open = false;
                                        });
                                    }else{
                                        DropboxService.getLink(this.props.dropbox.dropboxToken,x.id).then(resLink =>{
                                            downloadService.getInformation(resLink).then(resInfo => {
                                                let extension;
                                                let downloadType;
                                                let info = resInfo.getMediaProperties();
                                                if(info.format_name == "mp3"){
                                                    extension = "mp3";
                                                    downloadType = "Audios";

                                                    this.props.navigation.navigate('Download',{
                                                        urlDownload: resLink, 
                                                        downloadType: downloadType,
                                                        extension: extension
                                                    });
                                                }else if(info.format_name == "mov,mp4,m4a,3gp,3g2,mj2"){
                                                    extension = "mp4";
                                                    downloadType = "Videos";

                                                    this.props.navigation.navigate('Download',{
                                                        urlDownload: resLink, 
                                                        downloadType: downloadType,
                                                        extension: extension
                                                    });
                                                }else{
                                                    x.open = false;
                                                    this.setState({showAlert: true})
                                                }
                                                x.open = false;
                                            }).catch(errInfo => {
                                                this.setState({showAlert: true})
                                                x.open = false;
                                            })
                                        }).catch(err =>{
                                            this.setState({showAlert: true})
                                            x.open = false;
                                        });
                                    }
                                }}
                            />
                            <Divider />
                            </Layout>
                        ))}
                    </Layout>
                </ScrollView>

                {this.props.dropbox.hasMore ? 
                    <>
                        <Divider />
                        <Layout style={{padding:15}}>
                            <Button 
                                disabled={this.state.loadingButton}
                                status='basic'
                                onPress={()=>{
                                    this.setState({loadingButton: true});
                                    DropboxService.getFilesContinue(this.props.dropbox.dropboxToken,this.props.dropbox.dropboxNextToken).then(files =>{
                                        console.log(files);
                                        files.map((x)=> this.props.dropbox.dropboxList.push(x))
                                        store.dispatch({ type: "UPDATE_DROPBOX_LIST", dropboxList: this.props.dropbox.dropboxList });
                                        this.setState({loadingButton: false});
                                    }).catch(err =>{
                                        this.setState({showAlert: true})
                                    });
                                }}>
                                {this.state.loadingButton ? "Loading..." : "Load more"}
                            </Button>
                        </Layout>   
                    </>
                : null}

                <SnackBar 
					visible={this.state.showAlert} 
					messageColor="#FFFFFF"
					accentColor="#FFFFFF"
					textMessage="Request data is failed"
					backgroundColor={themeColor['color-danger-500']}
					actionHandler={()=>this.setState({showAlert: false})} 
					actionText="OK"
				/>
            </SafeAreaView>
        );
    }
}

function mapStateToProps(state) {
    return {
        init: state.init,
        dropbox: state.dropbox
    }
}
export default connect(mapStateToProps, null, null, { forwardRef: true })(DropBoxComponent);
