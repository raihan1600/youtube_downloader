import { InAppBrowser } from 'react-native-inappbrowser-reborn';
import axios from 'axios';
import * as _ from 'lodash';
import { store } from '@services/store';
import { getDeepLink } from '@services/utilities';

const dropbox_auth = "https://www.dropbox.com/oauth2";
const client_id = "55it4n4qg8z6t41";
const password = "lzuujiffexg1raf";
const redirect_uri = "https://medova.anurdin.net/success-login";
const qs = require('qs');
let accessToken = null;
let pageSize = 20;

const DropboxService = {
    dropboxSign: () => {
        return new Promise(async (resolve, reject) => {
            const deepLink = getDeepLink();
            const urlDropbox = `${dropbox_auth}/authorize?client_id=${client_id}&response_type=code&redirect_uri=${redirect_uri}`;
        
            if (await InAppBrowser.isAvailable()) {
                InAppBrowser.openAuth(urlDropbox, deepLink, {
                    ephemeralWebSession: false,
                    showTitle: false,
                    enableUrlBarHiding: true,
                    enableDefaultShare: false
                }).then(async (response) => {
                    console.log(response);
                    let query = response.url.split("?")[1];
                    let data = qs.parse(query);
                    if(data.code){
                        resolve();
                    }else{
                        reject();
                    }
                    let userInfo = await DropboxService.dropboxToken(data.code);
                    accessToken = userInfo.access_token;
                    store.dispatch({ type: "UPDATE_DROPBOX_TOKEN", dropboxToken: accessToken});
                    let files = await DropboxService.getFiles(accessToken,"");
                    store.dispatch({ type: "UPDATE_DROPBOX_LIST", dropboxList: files });
                }).catch((err) =>{
                    reject(err);
                });
            }
        });
    },
    dropboxToken: (code) => {
        return new Promise((resolve, reject) => {
            axios.post(`${dropbox_auth}/token`, qs.stringify({
                code: code,
                grant_type: "authorization_code",
                redirect_uri: redirect_uri,
            }), {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                auth: {
                    username: client_id,
                    password: password
                },
            }).then(res => {
                console.log(res);
                resolve(res['data']);
            }).catch(err => {
                console.log(err);
                reject(err);
            });
        });
    },
    
    getLink: (token, id) => {
        return new Promise((resolve, reject) => {
            var headers = {
                Authorization: `Bearer ${token}`
            }

            const body = {
                path: id,
            }
            axios.post(`https://api.dropboxapi.com/2/files/get_temporary_link`, body, { headers: headers }).then(r => {
                resolve(r.data.link)
            }).catch(e => {
                reject(e);
            });
        })
    },
    getFiles: (token, folderID) => {
        return new Promise((resolve, reject) => {
            var headers = {
                Authorization: `Bearer ${token}`
            }
            var body = {
                path: folderID,
                include_media_info: true,
                limit: pageSize,
                include_mounted_folders: true
            }
            axios.post(`https://api.dropboxapi.com/2/files/list_folder`, body, { headers: headers }).then(r => {
                store.dispatch({ type: "UPDATE_DROPBOX_NEXT_TOKEN", dropboxNextToken: r['data']['cursor'], hasMore: r['data']['has_more']});
                resolve(r['data']['entries']);
            }).catch(e => {
                reject(e);
            });
        });
    },
    getFilesContinue: (token, cursor) => {
        return new Promise((resolve, reject) => {
            var headers = {
                Authorization: `Bearer ${token}`
            }

            var body = {
                cursor: cursor
            }
            axios.post(`https://api.dropboxapi.com/2/files/list_folder/continue`, body, { headers: headers }).then(r => {
                store.dispatch({ type: "UPDATE_DROPBOX_NEXT_TOKEN", dropboxNextToken: r['data']['cursor'], hasMore: r['data']['has_more']});
                resolve(r['data']['entries']);
            }).catch(e => {
                reject(e);
            });
        });
    }
};

export default DropboxService;