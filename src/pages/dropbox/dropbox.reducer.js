export const dropboxReducer = (state = {
	dropboxList: [],
    dropboxToken: null,
    dropboxNextToken: null,
    hasMore: false,
	folderId: null,
	folderName: null
}, action) => {
	switch (action.type) {
		case "UPDATE_DROPBOX_FOLDER":
			state = {
				...state,
				folderId: action.folderId,
				folderName: action.folderName
			}
        break;
        case "UPDATE_DROPBOX_TOKEN":
			state = {
				...state,
				dropboxToken: action.dropboxToken
			}
        break;
		case "UPDATE_DROPBOX_NEXT_TOKEN":
			state = {
				...state,
				dropboxNextToken: action.dropboxNextToken,
                hasMore: action.hasMore
			}
        break;
		case "UPDATE_DROPBOX_LIST":
			state = {
				...state,
				dropboxList: action.dropboxList,
			}
        break;
	}
	return state;
}