export const initReducer = (state = {
	theme: "light",
}, action) => {
	switch (action.type) {
		case "UPDATE_THEME_LIGHT":
			state = {
				...state,
				theme: "light",
			}
        break;
		case "UPDATE_THEME_DARK":
			state = {
				...state,
				theme: "dark",
			}
        break;
	}
	return state;
}