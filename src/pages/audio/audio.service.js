
import { Text,Icon } from '@ui-kitten/components';
import React, { useState } from 'react';
import TrackPlayer, { 
	State, 
	Event,
	useProgress,
	useTrackPlayerEvents ,
} from 'react-native-track-player';
import { 
	MenuItem,
	OverflowMenu,
    Layout
} from '@ui-kitten/components';
import { View,Image,TouchableOpacity,Dimensions} from 'react-native';
import styles from './audio.style'
import mainStyles from '../../services/style';
import LottieView from 'lottie-react-native';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialComunity from 'react-native-vector-icons/MaterialCommunityIcons';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { store } from '@services/store';
const windowWidth = Dimensions.get('window').width;
const color = require('@assets/custom/custom-maping.json');

const CollectionServices = {
    
    // ========================================= //
    // TRACK DURATION
    // ========================================= //
    TrackDuration : () => {
        const progress = useProgress();
        const setPercentage = (progress.position/progress.buffered)*100;
        const getPercentage = isNaN(setPercentage) ? 0 : (setPercentage/10);

        return (
            <>
                <View style={{alignItems:'center'}}>
                    <MultiSlider
                        values={[getPercentage]}
                        sliderLength={windowWidth-30}
                        onValuesChangeFinish={(x)=> {
                            let getPosition = (progress.buffered * (x[0]*10))/100;
                            if(getPosition)TrackPlayer.seekTo(getPosition);
                        }}
                        step={0.01}
                        allowOverlap
                        snapped
                        markerStyle={styles.markerslide}
                        selectedStyle={styles.selectedStyle}
                    />
                </View>
                <View style={{justifyContent:'space-between',flexDirection:'row',paddingHorizontal:15}}>
                    <Text>{new Date(progress.position * 1000).toISOString().substring(19,11)}</Text>
                    <Text>{new Date(progress.buffered * 1000).toISOString().substring(19,11)}</Text>
                </View>
            </>
        );
    },

    // ========================================= //
    // TRACK PROGRESS
    // ========================================= //
    TrackProgress : () => {
        const progress = useProgress();
        const setPercentage = (progress.position/progress.buffered)*100;
        const getPercentage = isNaN(setPercentage) ? 0 : (setPercentage/10);

        return (
            <View style={{backgroundColor:'#EFEFEF'}}>
                <View style={{
                    height:5,
                    width:windowWidth*(getPercentage/10),
                    backgroundColor:color['color-primary-500']
                }}/>
            </View>
        );
    },

    // ========================================= //
    // TRACK DETAIL
    // ========================================= //
    TrackDetail : (props) => {
        const [trackTitle, setTrackTitle] = useState();
        const [trackArtWork, setTrackArtWork] = useState();
        const [trackArtist, setTrackArtist] = useState();

        useTrackPlayerEvents([Event.PlaybackTrackChanged], async event => {
            let track;
            if (event.type === Event.PlaybackTrackChanged && event.nextTrack != null) {
                if(event.nextTrack != undefined) track = await TrackPlayer.getTrack(event.nextTrack);
                const {title,artwork,artist} = track || {};
                setTrackTitle(title);
                setTrackArtWork(artwork);
                setTrackArtist(artist);
            }
        });

        TrackPlayer.getCurrentTrack().then(async res =>{
            let track;
            if(res != undefined) track = await TrackPlayer.getTrack(res);
            const {title,artwork,artist} = track || {};
            setTrackTitle(title);
            setTrackArtWork(artwork);
            setTrackArtist(artist);
        });
        
        if(props.type === "detail"){
            return(
                <>
                <View style={{padding:15, backgroundColor:"#F5F5F5"}}>
                    {trackArtWork ? 
                        <Image
                            style={[{borderRadius:4,width:windowWidth-30,height:201}]} 
                            source={{uri: trackArtWork}}
                        />
                    : 
                        <View style={{padding:10,borderRadius: 3, width: 100, paddingVertical: 50, alignSelf:'center'}}>
                            <Feather name='music' color='#3A3A3A' size={50}/>
                        </View>
                    }
                </View>
                <View style={{padding:15}}>
                    <Text 
                        category='h6' 
                        numberOfLines={2} 
                        style={{width:260,fontWeight:'500',paddingBottom:10}}
                    >
                        {trackTitle}
                    </Text>
                    <Text appearance='hint' numberOfLines={1} style={{paddingBottom:15}}>{trackArtist}</Text>
                </View>
                </>
            )
        }else if(props.type === "inline"){
            return(
                <>
                    {trackArtWork ?
                        <Image style={{borderRadius: 3, width: 55, height: 55}} source={{uri: trackArtWork}}/>
                    : 
                        <View style={{backgroundColor:'#EFEFEF', padding:10,borderRadius: 3}}>
                            <Feather name='music' color='#3A3A3A' size={35}/>
                        </View>
                    }
                    <View style={{paddingHorizontal:10,paddingTop:5}}>
                        <Text category='s1' numberOfLines={1} style={{marginBottom:5}}>{trackTitle}</Text>
                        <Text category='s2' numberOfLines={1} appearance={'hint'}>{trackArtist}</Text>
                    </View>
                </>
            );
        }
    },

    // ========================================= //
    // TRACK LIST
    // ========================================= //
    Indicator : (props) => {
        const [trackActivity, setTrackActivity] = useState();
        const [trackIndex, setTrackIndex] = useState();
        const [menuOpen, setOpenMenu] = useState(false);

        useTrackPlayerEvents([Event.PlaybackState], async event => {
            setTimeout(()=>{
                setTrackActivity(event.state);
            }, 100);
        });

        TrackPlayer.getState().then(res =>{
            setTrackActivity(res);
        });

        TrackPlayer.getCurrentTrack().then(res =>{
            setTrackIndex(res);
        });

        if(props.index === trackIndex && trackActivity === State.Playing && props.playlistOn){
            return(
                <LottieView 
                    style={{width:25,height:25}}
                    source={require('@assets/animation/lottieView.json')}
                    autoPlay
                    loop
                />
            );
        }else{
            return(
                <Layout style={{position:'relative', width:35}}>
                    <OverflowMenu
                        anchor={()=>(
                            <TouchableOpacity 
                                style={[mainStyles.wrapEditicon,{alignItems:'center'}]}
                                onPress={()=> setOpenMenu(true)}
                            >
                                <Icon style={{width:24,height:23}} fill='#8F9BB3' name='more-horizontal-outline'/>
                            </TouchableOpacity>
                        )}
                        backdropStyle={{backgroundColor: 'rgba(0, 0, 0, 0.5)'}}
                        visible={menuOpen}
                        onSelect={e => {
                            setOpenMenu(false);
                            props.onSelect(e.row);
                        }}
                        onBackdropPress={() => {
                            setOpenMenu(false);
                        }}
                        style={{width:200}}>
                        <MenuItem 
                            title={()=>(
                                <Text style={{
                                    textAlign:'left',
                                    width: '100%', 
                                    paddingLeft:10}}>
                                    Rename
                                </Text>
                            )}
                            accessoryLeft={()=> <Feather color='#3A3A3A' name='edit' size={16} style={{marginLeft:5}}/>}/>
                        <MenuItem 
                            title={()=>(
                                <Text style={{
                                    textAlign:'left',
                                    width: '100%', 
                                    paddingLeft:10
                                }}>
                                    Share
                                </Text>
                            )}
                            accessoryLeft={()=>  <Feather color='#3A3A3A' name='share-2' size={16} style={{marginLeft:5}}/>}     
                        />
                        <MenuItem 
                            title={()=>(
                                <Text style={{
                                    textAlign:'left',
                                    width: '100%', 
                                    paddingLeft:10}}>
                                    Add to playlist
                                </Text>
                            )}
                            accessoryLeft={()=>(
                                <MaterialIcons 
                                    name='playlist-add' 
                                    size={18} 
                                    color="#3A3A3A"
                                    style={{marginLeft:5}}
                                />
                            )}	
                            />
                        <MenuItem
                            disabled={props.disableMenu}
                            title={()=>(
                                <Text style={[
                                    {
                                        textAlign:'left',
                                        width: '100%', 
                                        paddingLeft:10
                                    }, 
                                    props.disableMenu ? {color: "#BBBBBB"} : null]}>
                                    Remove from Playlist
                                </Text>
                            )}
                            accessoryLeft={()=> <MaterialComunity color='#3A3A3A' name='playlist-remove' size={16} style={{marginLeft:5}}/>}  />
                        <MenuItem 
                            title={()=>(
                                <Text style={{
                                    color:color['color-danger-500'],
                                    textAlign:'left',
                                    width: '100%', 
                                    paddingLeft:10}}>
                                    Remove Track
                                </Text>
                            )}
                            accessoryLeft={()=> <Feather color='#3A3A3A' name='trash-2' size={16} style={{marginLeft:5}}/>}  />
                    </OverflowMenu>
                </Layout> 
            );
            
        }
    },

    // =============================== //
	// PLAY - PAUSE INDICATOR
	// =============================== //
	playPauseIndicator(props){
        const [trackActivity, setTrackActivity] = useState();

        useTrackPlayerEvents([Event.PlaybackState], async event => {
            setTrackActivity(event.state);
            props.onChange(event.state);
        });

        TrackPlayer.getState().then(res =>{
            setTrackActivity(res);
        });

        return (
            <Image 
                style={{paddingHorizontal:15,width: 55, height: 55}}
                source={trackActivity === State.Playing ? require("@assets/images/pause.png") : require("@assets/images/buttonplay.png")}
            />
        )
	},

    // =============================== //
	// PREV BUTTON
	// =============================== //
	prevButton(){
        const [btnDisabled, setDisabled] = useState(false);
        const [repeat, setRepeat] = useState();

        TrackPlayer.getRepeatMode().then(res =>{
            setRepeat(res);
        });

        TrackPlayer.getCurrentTrack().then(async (res) =>{
            if(res == 0){
                setDisabled(true);
            }else{
                setDisabled(false);
            }
        });

        return (
            <TouchableOpacity 
                disabled={btnDisabled && repeat != 2}
                style={styles.buttonIcon} 
                onPress={() => { 
                    TrackPlayer.skipToPrevious();
                    TrackPlayer.getCurrentTrack().then(async (index) =>{
                        let queue = await TrackPlayer.getQueue();
                        store.dispatch({ 
                            type: "UPDATE_AUDIO_TRACK", 
                            currentTrack: queue[index]
                        });
                    });
                }}>
                <MaterialIcons name='skip-previous' size={28} color={btnDisabled && repeat != 2 ? '#9F9F9F' : '#3A3A3A'}/>
            </TouchableOpacity>
        )
	},

    // =============================== //
	// NEXT BUTTON
	// =============================== //
	nextButton(){
        const [btnDisabled, setDisabled] = useState(false);
        const [repeat, setRepeat] = useState();

        TrackPlayer.getRepeatMode().then(res =>{
            setRepeat(res);
        });

        TrackPlayer.getCurrentTrack().then(async (res) =>{
            let queue = await TrackPlayer.getQueue();
            if(res == queue.length - 1){
                setDisabled(true);
            }else{
                setDisabled(false);
            }
        });

        return (
            <TouchableOpacity 
                disabled={btnDisabled && repeat != 2}
                style={styles.buttonIcon}
                onPress={()=> {
                    
                    TrackPlayer.skipToNext();
                    TrackPlayer.getCurrentTrack().then(async (index) =>{
                        let queue = await TrackPlayer.getQueue();
                        store.dispatch({ 
                            type: "UPDATE_AUDIO_TRACK", 
                            currentTrack: queue[index]
                        });
                    });
                }} 
            >
                <MaterialIcons name='skip-next' size={28} color={btnDisabled && repeat != 2 ? '#9F9F9F' : '#3A3A3A'}/>
            </TouchableOpacity>
        )
	}
	
}
export default CollectionServices;