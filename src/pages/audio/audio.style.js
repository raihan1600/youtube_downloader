import {StyleSheet, Dimensions} from 'react-native';
const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    wrapCurrentTeack:{
        paddingVertical:15, 
        flexDirection:'row',
        alignItems:'center', 
        width: windowWidth,
    },
    wrapCurrentTeackIcon:{
        flexDirection:'row',
        width: windowWidth-210, 
        alignItems: 'center', 
        justifyContent: 'center'
         
    },
    listAudio: {
        borderBottomColor: '#EAEBED',
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 15,
        borderBottomWidth: 1,
    },
    packIcon:{
        backgroundColor:'#F7F7F7',
        justifyContent:'center',
        width:32,
        height:32,
        borderRadius:100,
        alignItems:'center'
    },
    markerslide: {
        height: 18,
        width: 18,
        backgroundColor: '#FFFFFF',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4.84,
        elevation: 5,
    },
    selectedStyle: {
        backgroundColor: '#467DFC',
    },
    wrapIconAudio:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-evenly',
        marginBottom:30,
        marginTop:15
    },
    iconAudio:{
        width:14,
        height:16
    },
    buttonIcon:{
        width:28,
        height:28
    }
});

export default styles;
