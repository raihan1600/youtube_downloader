import React, { Component } from 'react';
import { 
	Divider, 
	TopNavigation, 
	Drawer, 
	Icon,
	Layout,
	ListItem,
	Text,
	Input,
	MenuItem,
	OverflowMenu
} from '@ui-kitten/components';
import {View,TouchableOpacity,SafeAreaView,Alert} from "react-native";
import { connect } from 'react-redux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { store } from '@services/store';
import TrackDetail from './track-detail';
import Feather from 'react-native-vector-icons/Feather';
import mainStyles from "@services/style";
import MainService from '@services/main';
import RBSheet from 'react-native-raw-bottom-sheet';
const color = require('@assets/custom/custom-maping.json');

class AudioPlaylistComponent extends Component {
	constructor(props) {
		super(props);
		this.state = {
			getId: null,
			rename:false,
			Visible:false,
			playlistData: [],
			name:'',
		};
	}

	componentDidMount(){		
		this.setState({playlistData: this.props.audio.playlistAudio});
	}

	// ====================== //
	// WRITE PLAYLIST DATA
	// ====================== //
	createPlaylist(){
		MainService.realmDb.write(() => {
			MainService.realmDb.create("Playlist", {
				_id: MainService.makeid(25),
				name: this.state.name,
				type: 'Audio'
			});
		});
		this.RBSheetInput.close();
		store.dispatch({ type: "UPDATE_UID_AUDIO", updateUIDAudio: MainService.makeid(10) });
		this.setState({
			getId:null,
			name:"",
		},()=>{
			this.setState({playlistData: this.props.audio.playlistAudio});
		});
	}

	// ====================== //
	// RENAME PLAYLIST 
	// ====================== //
	editPlaylist(){
		const data = MainService.realmDb.objects("Playlist").filter(obj => obj._id == this.state.getId.id)
		MainService.realmDb.write(() => {
			data.map((task) => task.name = this.state.name)
		})
		store.dispatch({ type: "UPDATE_UID_AUDIO", updateUIDAudio: MainService.makeid(10) });
		this.RBSheetInput.close();
		this.setState({
			getId:null,
			name:""
		},()=>{
			this.setState({playlistData: this.props.audio.playlistAudio});
		});
	}

	// ===================== //
	// DELETE PLAYLIST DATA //
	// ===================== //
	async deletePlaylist(e){
		MainService.realmDb.write(()=>{
			MainService.realmDb.delete(
				MainService.realmDb.objects("PlaylistData").filter(obj => obj._id == e),
			),
			MainService.realmDb.delete(
				MainService.realmDb.objects("Playlist").filter(obj => obj._id == e),
			)
		});
		store.dispatch({ type: "UPDATE_UID_AUDIO", updateUIDAudio: MainService.makeid(10) });
		this.setState({
			getId:null,
			name:"",
		},()=>{
			this.setState({playlistData: this.props.audio.playlistAudio});
		});
	}

	 render() {
		 return (
			<SafeAreaView style={{flex:1}}>
				<Layout style={mainStyles.container}>
					<TopNavigation 
						title={()=> <Text numberOfLines={1} category='s1'>Audio Playlist</Text>}
						alignment='center'
						accessoryRight={
							<TouchableOpacity onPress={() =>this.RBSheetInput.open()}>
								<MaterialIcons 
									name='playlist-add' 
									color='#3A3A3A' 
									size={26} 
									style={{paddingRight:15}}
								/>
							</TouchableOpacity>
						}
						/>
						<Divider />
						<Drawer>
						<ListItem 
							title={()=> <Text category='h6' style={mainStyles.textList}>All Songs</Text>}
							accessoryLeft={()=>(
								<View style={mainStyles.wrapFileicon}>
									<Feather name='music' color='#3A3A3A' size={20}/>
								</View>
							)}  
							onPress={(e)=>{ 
								this.props.navigation.navigate("ListAudio",{
									playlist:'',
									title: 'All Songs'
								});
								store.dispatch({ 
									type: "UPDATE_AUDIO_PLAYLIST", 
									playlistId: '',
									playlistTitle: 'All Songs'
								});
							}}
						/>
						{this.state.playlistData.map((x,i)=>(
							<ListItem 
								key={i}
								title={()=> <Text category='h6' style={mainStyles.textList} numberOfLines={1}>{x.name}</Text>}
								accessoryLeft={()=>(
									<View style={mainStyles.wrapFileicon}>
										<Feather name='music' color='#3A3A3A' size={20}/>
									</View>
								)}  
								accessoryRight={()=>(
									<View style={{position:'relative'}}>
									<OverflowMenu
										anchor={()=>(
											<TouchableOpacity 
												onPress={()=>{
													x.open = !x.open;
													this.setState({
														getId : x,
														playlistData:this.state.playlistData
													});
												}} 
												style={mainStyles.wrapEditicon}>
												<Icon name='more-horizontal-outline' fill='#8F9BB3'/>
											</TouchableOpacity>
										)}
										backdropStyle={{backgroundColor: 'rgba(0, 0, 0, 0.5)'}}
										visible={x.open}
										onSelect={e => {
											x.open = !x.open;
											this.setState({playlistData:this.state.playlistData});

											switch(e.row){
												case 0:
													this.setState({name: this.state.getId.name});
													this.RBSheetInput.open();
												break;
												case 1:
													Alert.alert('', `Are you sure you want to delete ${x.name}?`, [
														{
															text: "Cancel",
															onPress: () => null,
															style: "cancel"
														},
														{
															text: "Delete",
															onPress: () => this.deletePlaylist(this.state.getId.id),
															style: "destructive"
														}
													]);
												break;
											}
										}}
										onBackdropPress={() => {
											x.open = !x.open;
											this.setState({playlistData:this.state.playlistData});
										}}
										style={{width:200}}>
										<MenuItem 
											title={()=>(
												<Text style={{
													textAlign:'left',
													width: '100%', 
													paddingLeft:10}}>
													Rename
												</Text>
											)}
											accessoryLeft={()=> <Feather color='#3A3A3A' name='edit' size={16} style={{marginLeft:5}}/>}  />
										<MenuItem 
											title={()=>(
												<Text style={{
													color:color['color-danger-500'],
													textAlign:'left',
													width: '100%', 
													paddingLeft:10}}>
													Remove Playlist
												</Text>
											)}
											accessoryLeft={()=> <Feather color='#3A3A3A' name='trash-2' size={16} style={{marginLeft:5}}/>}/>
									</OverflowMenu>
									</View>
								)}
								onPress={(e)=>{ 
									this.props.navigation.navigate("ListAudio",{
										playlist:x['id'],
										title: x['name']
									});
									store.dispatch({ 
										type: "UPDATE_AUDIO_PLAYLIST", 
										playlistId: x['id'],
										playlistTitle: x['name']
									});
								}}
							/>
						))}
					</Drawer>	
							
					{/* ============ START RENAME TRACK LIST ============ */}
					<RBSheet
						ref={ref => {this.RBSheetInput = ref}}
						height={250}
						closeOnDragDown={false}
						closeOnPressMask={true} 
						openDuration={500}
						onClose={()=>{
							this.setState({
								getId: null,
								name: ''
							});
						}}
						>
						<TopNavigation 
							style={{borderTopLeftRadius:20,borderTopRightRadius:20}}
							title={this.state.getId ?  "Rename Playlist" : "Create Playlist"}
							alignment='center'
							accessoryLeft={()=> (
								<Icon 
									fill='#3A3A3A' 
									name='arrow-back' 
									size={28}
									onPress={()=> {
										this.RBSheetInput.close(),
										this.setState({
											getId: null,
											name: ''
										});
									}}
									style={mainStyles.iconTopNavigation}
								/>
							)}
							accessoryRight={()=> (
								<TouchableOpacity 
									style={{paddingRight:15}} 
									onPress={()=>{ 
										if(this.state.getId){
											this.editPlaylist();
										}else{
											this.createPlaylist();
										}}}>
									<Text category='s1' style={{fontWeight:'500'}} status='primary' >
										Save
									</Text>
								</TouchableOpacity>
							)}
						/>
						<Divider/>
						<Layout style={{paddingHorizontal:15}}>
							<View>
								<Feather name='music' color='#3A3A3A' size={50} style={{paddingVertical:15,alignSelf:'center'}} />
								<Text style={{paddingBottom:15,textAlign:'center'}} numberOfLines={1}>{this.state.name ? this.state.name : '...'}</Text>
							</View>
							<Divider/>
							<View>
								<Input 
									placeholder='Enter playlist name'
									defaultValue={this.state.getId ? this.state.getId.name : ''} 
									onChangeText={(x)=>this.setState({name:x})} />
							</View>
						</Layout>
					</RBSheet>
					{/* ============ END RENAME TRACK LIST ============ */}
				</Layout>

				<TrackDetail openList={()=> {
					this.props.navigation.navigate("ListAudio",{
						playlist: this.props.audio.playlistIdOnTrack,
						title: this.props.audio.playlistTitle
					});
				}}/>
			</SafeAreaView>
		)
	}
}

function mapStateToProps(state) {
    return {
        init: state.init,
        download: state.download.downloadData,
		audio: state.audio
    }
}
export default connect(mapStateToProps, null, null, { forwardRef: true })(AudioPlaylistComponent);