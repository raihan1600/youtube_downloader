import React, { Component } from 'react';
import { View,SafeAreaView,Image,TouchableOpacity,ScrollView,Dimensions, Alert, Share} from 'react-native';
import { 
	TopNavigation, 
	Divider,
	Layout,
	Text,
	Drawer,
	ListItem,
	Input,
	Icon
} from '@ui-kitten/components';
import TrackPlayer, { State, RepeatMode } from 'react-native-track-player';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import RBSheet from 'react-native-raw-bottom-sheet';
import Feather from 'react-native-vector-icons/Feather';
import CollectionServices from './audio.service';
import TrackDetail from './track-detail';
import MainService from '@services/main';
import mainStyles from '@services/style';
import findIndex  from 'lodash/findIndex';
import { connect } from 'react-redux';
import { store } from '@services/store';
import styles from './audio.style'

const RNFS = require('react-native-fs');
const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

// ========================================= //
// MAIN COMPPONENT
// ========================================= //
class AudioListComponent extends Component {
	constructor(props) {
		super(props);
		this.state = {
			audio :[],
			getId:null,
			repeatMode: 0,
			selectItem: null,
			currentTrack: null,
			selectPlaylist: null,
			isPlay: false,
			visible:false,
			disableMenu:true,
			licensed:'',
			path:'',
			name:"",
			title:'',
			pathThumbnail:'',
			showDetail: true		};
	}

	componentDidMount(){
		this.disableFromPlaylist();

		TrackPlayer.getRepeatMode().then(res => {
			this.setState({repeatMode: res});
		});
	}

	// =============================== //
	// DELETE TRACK FROM LIST
	// =============================== //
	async deleteFromPlaylist(i,id){
		MainService.realmDb.write(() =>
			MainService.realmDb.delete(
				MainService.realmDb.objects('PlaylistData').filter(obj => obj.media_id == id)
			),
		);

		TrackPlayer.remove([i]);

		store.dispatch({ 
			type: "UPDATE_UID_TRACK", 
			updateUIDTrack: MainService.makeid(25)
		});
	}

	// ====================== //
	// RENAME AUDIOS 
	// ====================== //
	editAudios(){
		const data = MainService.realmDb.objects("Audios").filter(obj => obj._id == this.state.getId)
		MainService.realmDb.write(() => {
			data.map((task) => task.title = this.state.name)
		});
		this.state.audio.map((x)=>{
			if(x.id == this.state.getId) x.title = this.state.name;
		})
		this.setState({ getId:null });
		this.RBSheetInput.close();
		store.dispatch({ type: "UPDATE_UID_TRACK", updateUIDTrack: MainService.makeid(25)});
	}
	
	// =============================== //
	// DELETE AUDIO
	// =============================== //
	 async deleteAudioFile() {
		TrackPlayer.getQueue().then(async (list) =>{
			let index = list.findIndex(x => x.id === this.state.getId);
			TrackPlayer.remove([index]);

			if(this.props.audio.currentTrack){
				if(this.state.getId == this.props.audio.currentTrack.id){
					TrackPlayer.getState().then(res =>{
						if(res != State.Playing){
							store.dispatch({ 
								type: "UPDATE_AUDIO_TRACK", 
								currentTrack: null
							});
						}
					});
				}
			}

			const dirAudio = this.state.path;
			const dirthumbnail = this.state.pathThumbnail;
			const filepath = `${dirAudio}`; 
			const filethumbnail = `${dirthumbnail}`;
			let existsPath = await RNFS.exists(filepath);
			if(existsPath) RNFS.unlink(filepath);

			let existsThumb = await RNFS.exists(filethumbnail);
			if(existsThumb) RNFS.unlink(filethumbnail);

			MainService.realmDb.write(() =>
				MainService.realmDb.delete(
					MainService.realmDb.objects('Audios').filter(obj => obj._id == this.state.getId),
				),
			);
			MainService.realmDb.write(() =>
				MainService.realmDb.delete(
					MainService.realmDb.objects('PlaylistData').filter(obj => obj.media_id == this.state.getId)
				),
			);
			this.setState({visible:false});
			store.dispatch({ type: "UPDATE_UID_TRACK", updateUIDTrack: MainService.makeid(25)});
		});
	}	 
	
	// =============================== //
	// PLAY TRACK PLAYER
	// =============================== //
	async playTrackPlayer(index,licensed){
		if(this.props.route.params.playlist == this.props.audio.playlistIdOnTrack){
			TrackPlayer.seekTo(0);
			TrackPlayer.skip(index).then(()=>{
				TrackPlayer.play();
				this.setState({
					licensed:licensed,
					isPlay:true,
					currentTrack: this.props.audio.trackAudio[index]
				});

				store.dispatch({ 
					type: "UPDATE_AUDIO_TRACK", 
					currentTrack: this.props.audio.trackAudio[index]
				});
			});
		}else{
			store.dispatch({ 
				type: "UPDATE_AUDIO_PLAYLIST_ON_TRACK", 
				playlistIdOnTrack: this.props.route.params.playlist,
				playlistTitleOnTrack: this.props.route.params.title
			});
			await TrackPlayer.setupPlayer({});
			await TrackPlayer.reset();
			TrackPlayer.add(this.props.audio.trackAudio);
			TrackPlayer.skip(index).then(()=>{
				TrackPlayer.play();
				this.setState({
					licensed:licensed,
					isPlay:true,
					currentTrack: this.props.audio.trackAudio[index]
				});

				store.dispatch({ 
					type: "UPDATE_AUDIO_TRACK", 
					currentTrack: this.props.audio.trackAudio[index]
				});
			});
		}
	}

	// ====================== //ab
	// ADD PLAYLIST DATA
	// ====================== //
	addToPlaylist(){
		MainService.realmDb.write(() => {
			MainService.realmDb.create("PlaylistData", {
				_id: MainService.makeid(25),
				playlist_id: this.state.selectPlaylist.id,
				media_id: this.state.selectItem.id
			});
		});
	}

	// ====================== //
	// Hide menu remove
	// ====================== //
	disableFromPlaylist(){
		if(this.props.route.params.playlist){
			this.setState({disableMenu:false})
		}else{
			this.setState({disableMenu:true})
		}
	}
	
	render() {
		return (
			<SafeAreaView style={mainStyles.container}>
				{/* ========== START HEADER ========== */}
				<TopNavigation 
					title={()=> <Text numberOfLines={1} category='s1' style={{width:windowWidth * 0.7,textAlign:'center'}}>{this.props.route.params.title}</Text>}
					subtitle={this.props.route.params['folder']}
					alignment='center'
					accessoryLeft={()=> (
					<MaterialIcons 
						color='#3A3A3A' 
						name='arrow-back' 
						size={28}
						onPress={()=> this.props.navigation.goBack()}
						style={mainStyles.iconTopNavigation}
					/>
				)}
				accessoryRight={()=> (
					<Layout style={{flexDirection:'row'}}>
						{/* =========== START REPEAT MODE =========== */}
						<TouchableOpacity onPress={()=>{
							this.setState({showDetail: false});
							TrackPlayer.getRepeatMode().then(res => {
								switch(res){
									case RepeatMode.Off:
										TrackPlayer.setRepeatMode(RepeatMode.Queue);
										this.setState({repeatMode: RepeatMode.Queue});
									break;
									case RepeatMode.Queue:
										TrackPlayer.setRepeatMode(RepeatMode.Track);
										this.setState({repeatMode: RepeatMode.Track});
									break;
									case RepeatMode.Track:
										TrackPlayer.setRepeatMode(RepeatMode.Off);
										this.setState({repeatMode: RepeatMode.Off});
									break;
								}
								this.setState({showDetail: true});
							});
						}}>
							{this.state.repeatMode === RepeatMode.Off ? <MaterialIcons name='repeat' color='#3A3A3A' size={28}/>:null}
							{this.state.repeatMode === RepeatMode.Track ? <MaterialIcons name='repeat-one' color='#3A3A3A' size={28}/>:null}
							{this.state.repeatMode === RepeatMode.Queue ? <MaterialIcons name='repeat-on' color='#3A3A3A' size={28}/>:null}
						</TouchableOpacity>
						{/* =========== REPEAT MODE =========== */}
					</Layout>
				)}
				/>
				{/* ========== END HEADER ========== */}

				<Divider />

				{/* ========== START TRACK LIST ========== */}
				<ScrollView>
					<Layout style={{flex:1}}>
						{this.props.audio.trackAudio.map((data,i) => (
							<TouchableOpacity 
								key={i} 
								style={styles.listAudio} 
								onPress={async()=> {
									this.playTrackPlayer(i,data.licensed);
								}}>
								<View style={{flexDirection:'row'}}>
									{data.artwork ? 
										<Image source={{ uri: data.artwork}} style={{width: 70, height: 70, borderRadius: 3}}/>
									: 
										<View style={{backgroundColor:'#EFEFEF', width:70,height:70, padding:15,borderRadius: 3}}>
											<Feather name='music' color='#3A3A3A' size={40}/>
										</View>
									}
									
									<View style={{ paddingLeft: 10}}>
										<Text category='s1' numberOfLines={2} style={{width:205, marginBottom:5}}>{data.title}</Text>
										<Text category='s1' style={{paddingTop:5}} appearance='hint'>{MainService.convertSeconds(data.duration)}</Text>
									</View>
								</View>
								<CollectionServices.Indicator 
									onSelect={(e)=> {
										this.setState({
											selectItem: data,
											path:data.url,
											getId:data.id,
											pathThumbnail:data.artwork,
											title:data.title,
										}, ()=>{
											switch(e){
												case 0:
													this.RBSheetInput.open();
												break;
												case 1:
													Share.share({
														title: data.title,
														url: data.url
													});
												break;
												case 2:
													this.rbSheetPlaylist.open();
												break;
												case 3:
													Alert.alert('', `Are you sure you want to delete ${data.title} from playlist?`, [
														{
															text: "Cancel",
															onPress: () => null,
															style: "cancel"
														},
														{
															text: "Delete",
															onPress: () => this.deleteFromPlaylist(i,this.state.getId),
															style: "destructive"
														}
													]);
												break;
												case 4:
													Alert.alert('', `Are you sure you want to delete ${data.title}?`, [
														{
															text: "Cancel",
															onPress: () => null,
															style: "cancel"
														},
														{
															text: "Delete",
															onPress: () => this.deleteAudioFile(this.state.getId),
															style: "destructive"
														}
													]);
												break;
											}
										});
									}} 
									playlistOn={this.props.route.params.playlist == this.props.audio.playlistIdOnTrack}
									disableMenu={this.state.disableMenu}
									index={i}
								/>
							</TouchableOpacity>
						))}
					</Layout>
				</ScrollView>
				{/* ========== END HEADER ========== */}

				{this.state.showDetail ? <TrackDetail openList={()=> {
					if(this.props.audio.playlistIdOnTrack !== this.props.audio.playlistId){
						setTimeout(()=>{
							this.props.navigation.navigate("ListAudio",{
								playlist: this.props.audio.playlistIdOnTrack,
								title: this.props.audio.playlistTitleOnTrack
							});
	
							store.dispatch({ 
								type: "UPDATE_AUDIO_PLAYLIST", 
								playlistId: this.props.audio.playlistIdOnTrack,
								playlistTitle: this.props.audio.playlistTitleOnTrack
							});
						}, 500);
					}
				}}/> : null}

				{/* =========== START PLAYLIST =========== */}
				<RBSheet
					ref={ref => {this.rbSheetPlaylist = ref}}
					height={windowHeight - 100}
					closeOnDragDown={true}
					closeOnPressMask={true} 
					openDuration={50}
					closeDuration={50}>

					<SafeAreaView>
						<Divider/>
						<TopNavigation
							title="Select Playlist"
							alignment='center'
							accessoryLeft={()=> (
								<MaterialIcons 
									fill='#3A3A3A' 
									name='arrow-back' 
									size={22}
									onPress={()=>{
										this.rbSheetPlaylist.close();
									}}
								/>
							)}
						/>
						<Divider/>
						<Layout style={{height:windowHeight - 183}}>
							<Drawer>
								{this.props.audio.playlistAudio.map((x,i)=>(
									<ListItem 
										key={i}
										title={()=> <Text category='h6' style={mainStyles.textList} numberOfLines={1}>{x.name}</Text>}
										accessoryLeft={()=> <MaterialIcons color='#8F9BB3' name='playlist-add' size={20} style={{marginLeft:5}}/>}  
										onPress={()=> {
											this.setState({selectPlaylist: x},()=>{
												this.rbSheetPlaylist.close();
												this.addToPlaylist();
											});
										}}
									/>
								))}
							</Drawer>
						</Layout>
						<Divider/>
					</SafeAreaView>
				</RBSheet>
				{/* =========== END PLAYLIST =========== */}

				{/* =========== START RENAME AUDIO =========== */}
				<RBSheet
					ref={ref => {this.RBSheetInput = ref}}
					height={260}
					closeOnDragDown={false}
					closeOnPressMask={true} 
					openDuration={500}
					>
					<SafeAreaView>
						<TopNavigation 
							style={{borderTopLeftRadius:20,borderTopRightRadius:20}}
							title=  "Rename Audio" 
							alignment='center'
							accessoryLeft={()=> (
								<Icon 
									fill='#3A3A3A' 
									name='arrow-back' 
									size={28}
									onPress={()=> {
										this.RBSheetInput.close(),
										this.setState({
											getId: null,
											name: ''
										});
									}}
									style={mainStyles.iconTopNavigation}
								/>
							)}
							accessoryRight={()=> (
								<TouchableOpacity 
									style={{paddingRight:15}} 
									onPress={()=>this.editAudios()}>
									<Text category='s1' style={{fontWeight:'500'}} status='primary' >
										Save
									</Text>
								</TouchableOpacity>
							)}
						/>
						<Divider/>
						<Layout style={{paddingHorizontal:15}}>
							<View>
								{this.state.pathThumbnail ? 
									<Image style={{width:80,height:60,alignSelf:'center',margin:15,borderRadius:5}} source={{uri:this.state.pathThumbnail}}/>
								: 
									<View style={{backgroundColor:'#EFEFEF', padding:10,borderRadius: 3,margin:15, alignSelf:'center'}}>
										<Feather name='music' color='#3A3A3A' size={35}/>
									</View>	
								}
								<Text style={{paddingBottom:15,textAlign:'center'}} numberOfLines={1}> {this.state.name == "" ? this.state.title :this.state.name}</Text>
							</View>
							<Divider/>
							<View>
								<Input 
									placeholder='Enter audio name'
									defaultValue={this.state.selectItem ? this.state.selectItem.title : ''} 
									onChangeText={(x)=>this.setState({name:x})} />
							</View>
						</Layout>
					</SafeAreaView>
				</RBSheet>
				{/* =========== END RENAME AUDIO =========== */}
			</SafeAreaView>
			)
		}
	}

function mapStateToProps(state) {
    return {
        init: state.init,
        audio: state.audio
    }
}
export default connect(mapStateToProps, null, null, { forwardRef: true })(AudioListComponent);