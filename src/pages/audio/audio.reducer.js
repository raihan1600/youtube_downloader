export const audioReducer = (state = {
	playlistAudio: [],
	trackAudio: [],
	originTrackAudio: [],
	updateUIDAudio: null,
	updateUIDTrack: null,
	playlistId: '',
	playlistTitle: '',
	playlistIdOnTrack: null,
	playlistTitleOnTrack: '',
	currentTrack: null
}, action) => {
	switch (action.type) {
		case "UPDATE_AUDIO_TRACK":
			state = {
				...state,
				currentTrack: action.currentTrack,
			}
        break;
		case "UPDATE_AUDIO_PLAYLIST":
			state = {
				...state,
				playlistId: action.playlistId,
				playlistTitle: action.playlistTitle
			}
        break;
		case "UPDATE_AUDIO_PLAYLIST_ON_TRACK":
			state = {
				...state,
				playlistIdOnTrack: action.playlistIdOnTrack,
				playlistTitleOnTrack: action.playlistTitleOnTrack
			}
        break;
		case "UPDATE_TRACK_AUDIO":
			state = {
				...state,
				trackAudio: action.trackAudio,
			}
        break;
		case "UPDATE_ORIGIN_TRACK_AUDIO":
			state = {
				...state,
				originTrackAudio: action.originTrackAudio,
			}
        break;
		case "UPDATE_PLAYLIST_AUDIO":
			state = {
				...state,
				playlistAudio: action.playlistAudio,
			}
        break;
		case "UPDATE_UID_AUDIO":
			state = {
				...state,
				updateUIDAudio : action.updateUIDAudio,
			}
		break;
		case "UPDATE_UID_TRACK":
			state = {
				...state,
				updateUIDTrack : action.updateUIDTrack,
			}
		break;
	}
	return state;
}