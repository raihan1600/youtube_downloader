import React, { Component } from 'react';
import { View, TouchableOpacity, Linking, SafeAreaView } from 'react-native';
import { 
	Layout
} from '@ui-kitten/components';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import RBSheet from 'react-native-raw-bottom-sheet';
import TrackPlayer, { State, RepeatMode } from 'react-native-track-player';
import { connect } from 'react-redux';
import { store } from '@services/store';
import CollectionServices from './audio.service';
import styles from './audio.style'

class TrackDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        if(this.props.audio.currentTrack ){
            return (
                <>
                {/* =========== START TRACK =========== */}
                <CollectionServices.TrackProgress />
                <Layout style={styles.wrapCurrentTeack}>
                    <TouchableOpacity 
                        style={{ flexDirection:'row',width: 210, paddingLeft: 15 }} 
                        onPress={() => {this.RBSheet.open()}}
                    >
                        <CollectionServices.TrackDetail type="inline"/>
                    </TouchableOpacity>
                    <Layout style={styles.wrapCurrentTeackIcon}>
                        <CollectionServices.prevButton/>
                        <TouchableOpacity 
                            style={{ width:84 ,alignItems: 'center'	}}
                            onPress={async() => {
                                
                                const state = await TrackPlayer.getState();
                                
                                if (state === State.Playing) {
                                    TrackPlayer.pause();
                                    this.setState({isPlay:false});
                                }
                                
                                if(state === State.Paused){
                                    const position = await TrackPlayer.getPosition();
                                    const duration = await TrackPlayer.getDuration();
                                    if(duration.toFixed(0) == position.toFixed(0)){
                                        let track = await TrackPlayer.getCurrentTrack();
                                        if(track === this.props.audio.trackAudio.length -1){
                                            TrackPlayer.seekTo(0);
                                            TrackPlayer.play();
                                        }
                                    }else{
                                        TrackPlayer.play();
                                    }
                                    this.setState({isPlay:true});
                                }}
                        }>
                            <CollectionServices.playPauseIndicator onChange={(e)=> this.setState({isPlay: e === State.Playing})} />
                        </TouchableOpacity>
                        <CollectionServices.nextButton/>
                    </Layout>
                </Layout>
                {/* =========== END TRACK =========== */}

                {/* =========== START TRACK DETAIL =========== */}
                <RBSheet
					ref={ref => {this.RBSheet = ref}}
					height={510}
					closeOnDragDown={true}
					closeOnPressMask={true} 
					openDuration={500}
					customStyles={{
					container: {
						justifyContent: "center",
						alignItems: "center"
					}}
				}>
                    <SafeAreaView>
                        <Layout style={{flex:1}}>
                            <CollectionServices.TrackDetail type="detail"/>
                            <Layout style={{flex:1,justifyContent:'flex-end'}}>
                                <CollectionServices.TrackDuration />
                                <View style={styles.wrapIconAudio}>
                                    <TouchableOpacity onPress={()=> Linking.openURL(this.props.audio.currentTrack.licensed)}>
                                        <MaterialIcons name='link' size={28} color='#3A3A3A'/>
                                    </TouchableOpacity>
                                    <CollectionServices.prevButton/>
                                    <TouchableOpacity 
                                        style={{ width:84 ,alignItems: 'center'	}}
                                        onPress={async() => {
                                        const state = await TrackPlayer.getState();
                                        if (state === State.Playing) {
                                            TrackPlayer.pause();
                                            this.setState({isPlay:false});
                                        }else{
                                            TrackPlayer.play();
                                            this.setState({isPlay:true});
                                        }}}
                                    >
                                        <CollectionServices.playPauseIndicator onChange={(e)=> this.setState({isPlay: e === State.Playing})} />
                                    </TouchableOpacity>
                                    <CollectionServices.nextButton/>
                                    <TouchableOpacity onPress={()=>{
                                        TrackPlayer.getRepeatMode().then(res => {
                                            switch(res){
                                                case RepeatMode.Off:
                                                    TrackPlayer.setRepeatMode(RepeatMode.Queue);
                                                    this.setState({repeatMode: RepeatMode.Queue});
                                                break;
                                                case RepeatMode.Queue:
                                                    TrackPlayer.setRepeatMode(RepeatMode.Track);
                                                    this.setState({repeatMode: RepeatMode.Track});
                                                break;
                                                case RepeatMode.Track:
                                                    TrackPlayer.setRepeatMode(RepeatMode.Off);
                                                    this.setState({repeatMode: RepeatMode.Off});
                                                break;
                                            }
                                        });
                                    }}>
                                        {this.state.repeatMode === RepeatMode.Off ? <MaterialIcons name='repeat' size={28} color='#3A3A3A'/>:null}
                                        {this.state.repeatMode === RepeatMode.Track ? <MaterialIcons name='repeat-one' size={28} color='#3A3A3A'/>:null}
                                        {this.state.repeatMode === RepeatMode.Queue ? <MaterialIcons name='repeat-on' size={28} color='#3A3A3A'/>:null}
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=>{
                                        this.props.openList();
                                        this.RBSheet.close();
                                    }}>
                                        <MaterialIcons name='queue-music' size={28} color='#3A3A3A'/>
                                    </TouchableOpacity>
                                </View>
                            </Layout>
                        </Layout>
                    </SafeAreaView>
				</RBSheet>
                {/* =========== END TRACK DETAIL =========== */}
                </>
            );
        }else{
            return null;
        }
    }
}

function mapStateToProps(state) {
    return {
        audio: state.audio
    }
}
export default connect(mapStateToProps, null, null, { forwardRef: true })(TrackDetail);
