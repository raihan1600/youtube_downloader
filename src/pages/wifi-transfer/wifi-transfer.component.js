import React, { Component } from 'react';
import { SafeAreaView, ActivityIndicator } from 'react-native';
import { 
	Divider, 
	Text, 
	TopNavigation, 
    Layout,
    Button
} from '@ui-kitten/components';
import * as _ from 'lodash';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import mainStyles from '@services/style';
import MainService from '@services/main';
import SnackBar from 'react-native-snackbar-component';
import { HttpServer } from "@react-native-library/webserver";
import { NetworkInfo } from "react-native-network-info";
import CameraRoll from "@react-native-community/cameraroll";
import axios from 'axios';
const RNFS = require('react-native-fs');
const color = require('@assets/custom/custom-maping.json');

class WifiTransferComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            url: null,
            port: 4000,
            showAlert: false,
            message: '',
            alertStatus: 'success',
            isUploading: false,
            totalFile: 0,
            uploadTask: []
        };
    }

    async componentDidMount() {
        let css = await RNFS.readFile(`${RNFS.DocumentDirectoryPath}/www/style.css`,'utf8');
        let js = await RNFS.readFile(`${RNFS.DocumentDirectoryPath}/www/script.js`,'utf8');
        let html = await RNFS.readFile(`${RNFS.DocumentDirectoryPath}/www/index.html`,'utf8');

        NetworkInfo.getIPV4Address().then(ipv4Address => {
            this.setState({
                url: ipv4Address
            })
        });

        HttpServer.start(this.state.port, 'http_service', (request, response) => {
            console.log(request['headers']);
            // ================================= //
            // UPLOAD PROCESS
            // ================================= //
            if(request.method === "GET" && request.url.startsWith('/upload')){
                let groupDir = request['headers']['type'].includes("audio") ? "Audios" : "Videos";
                let filename = `${request['headers']['name'].split(".")[0]}-${request['requestId']}`;
                let filePath = `${RNFS.DocumentDirectoryPath}/${groupDir}/${filename}`;
                RNFS.writeFile(`${filePath}.${groupDir == "Audios" ? "mp3" : "mp4"}`, request['headers']['file'].split('base64,')[1], 'base64').then(async (success) => {
                    let finish = ()=>{
                        response.send(200, "application/json", "{\"message\": \"OK\"}");
                        this.state.uploadTask.push(request['headers']['name']);
                        this.setState({
                            uploadTask: this.state.uploadTask
                        }, ()=>{
                            if(this.state.totalFile == this.state.uploadTask.length){
                                this.setState({
                                    isUploading: false,
                                    showAlert: true,
                                    message: 'Upload file success',
                                    alertStatus: 'success'
                                });
                                setTimeout(()=>{
                                    this.setState({showAlert: false});
                                }, 3000);
                            }
                        });
                    };

                    if(groupDir === "Audios"){
                        let info = await MainService.getInformation(`${filePath}.${groupDir == "Audios" ? "mp3" : "mp4"}`);
                        let media = {
                            "title": _.capitalize(_.lowerCase(request['headers']['name'].split(".")[0])),
                            "category": "",
                            "category_url": "",
                            "artist": "",
                            "artist_url": "",
                            "duration": info.getMediaProperties().duration,
                            "licensed": "",
                            "thumbnail": "",
                            "content_length": Number(request['headers']['size']),
                            "path": `${filePath}.${groupDir == "Audios" ? "mp3" : "mp4"}`
                        };
                        MainService.writeFileData(groupDir,media).then(resData =>{
                            finish();
                        });
                    }else{
                        CameraRoll.save(`file://${filePath}.mp4`, { type: "video", album: "Medova" }).then(resSave =>{
                            RNFS.unlink(`${filePath}.mp4`).then((resUnlink) => {
                                finish();
                            });
                        });
                    }
                }).catch((err) => {
                    console.log(err);
                    response.send(400, "application/json", "{\"message\": \"BAD\"}");
                });

            // ================================= //
            // ADD QUEUE UPLOAD
            // ================================= //
            }else if(request.method === "GET" && request.url.startsWith('/add-upload')){
                this.setState({
                    isUploading: true,
                    totalFile: request['query']['total'],
                    uploadTask: []
                });
                response.send(200, "text/javascript","{\"message\": \"OK\"}");

            // ================================= //
            // CREATE STATIC STYLE.CSS
            // ================================= //
            }else if(request.method === "GET" && request.url.startsWith('/style.css')){
                response.send(200, "text/css",css);

            // ================================= //
            // CREATE STATIC SCRIPT.JS
            // ================================= //
            }else if(request.method === "GET" && request.url.startsWith('/script.js')){
                response.send(200, "text/javascript",js);
            
            // ================================= //
            // DETECT DEVICE CONNECTED
            // ================================= //
            }else if(request.method === "GET" && request.url.startsWith('/register')){
                this.setState({
                    showAlert: true,
                    message: 'Device detected',
                    alertStatus: 'info'
                }, ()=>{
                    setTimeout(()=>{
                        this.setState({showAlert: false});
                    }, 3000);
                });
                response.send(200, "text/javascript","{\"message\": \"OK\"}");

            // ================================= //
            // RENDER HTML HOME PAGE
            // ================================= //
            }else{
                response.send(200, "text/html",html);
            }
        });
    }

    componentWillUnmount() {
        HttpServer.stop();
    }

    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
				<TopNavigation 
					title={()=> <Text category='s1'>Wifi Transfer</Text>}
					alignment='center'
					accessoryLeft={()=> (
						<MaterialIcons 
							color='#3A3A3A' 
							name='arrow-back' 
							size={28}
							onPress={()=> {
                                this.props.navigation.goBack()
                            }}
							style={mainStyles.iconTopNavigation}
						/>
					)}
				/>
				<Divider />
                <Layout style={{flex:1}}>
                    <Layout style={{
                         paddingHorizontal: 30,
                         paddingVertical: 30,
                         backgroundColor: '#EFEFEF',
                         marginBottom: 30
                    }}>
                        <Text category="p1" style={{marginBottom: 5}}>
                            Device and computers need to be in same network
                        </Text>
                        <Text category="p1" style={{marginBottom: 30}}>
                            On your computer's browser go to:
                        </Text>
                        <Text style={{
                                textAlign: 'center',
                                fontSize: 20,
                                marginBottom: 50,
                                color: color['color-primary-500']
                            }}
                            category='s1'>
                            http://{this.state.url}:{this.state.port}
                        </Text>
                        <Text category="p1">
                            Please note that if you close this page, the transfer will stop
                        </Text>
                    </Layout>
                        
                    {this.state.isUploading ? 
                    <Layout style={{alignItems: 'center'}}>
                        <ActivityIndicator style={{marginBottom: 15}} size="large" />
                        <Text category="h6">Device Uploading... {this.state.uploadTask.length}/{this.state.totalFile}</Text>
                    </Layout>
                    : null}
                    
                </Layout>
                <SnackBar 
					visible={this.state.showAlert} 
					messageColor="#FFFFFF"
					accentColor="#FFFFFF"
					textMessage={this.state.message}
					backgroundColor={color['color-'+ this.state.alertStatus +'-500']}
					actionHandler={()=>this.setState({showAlert: false})} 
					actionText="OK"
				/>
            </SafeAreaView>
        );
    }
}

export default WifiTransferComponent;
