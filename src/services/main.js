import Realm from "realm";
import AudioSchema from "@schema/audio";
import PlaylistSchema from "@schema/playlist";
import PlaylistDataSchema from "@schema/playlist-data";
import { RNFFmpeg,RNFFprobe } from 'react-native-ffmpeg';
import { store } from '@services/store';
const RNFS = require('react-native-fs');

const MainService = {
    realmDb: null,

    // ====================== //
	// OPEN REALM DB
	// ====================== //
    openDb: async ()=>{
        MainService.realmDb = await Realm.open({
            path: "medovaDB",
            schema: [AudioSchema,PlaylistSchema,PlaylistDataSchema],
        });
    },

    // ====================== //
	// CREATE RANDOM ID
	// ====================== //
	makeid(length) {
		var result           = '';
		var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		var charactersLength = characters.length;
		for ( var i = 0; i < length; i++ ) {
		result += characters.charAt(Math.floor(Math.random() * 
			charactersLength));
		}
		
		return result;
	},
    
	// ====================== //
	// CONVERT SECOND TO TIME
	// ====================== //
	convertSeconds(seconds) {
        var convert = function(x) { return (x < 10) ? "0"+x : x; }
        return convert(parseInt(Number(seconds) / (60*60))) + ":" +
                convert(parseInt(Number(seconds) / 60 % 60)) + ":" +
                convert(parseInt(Number(seconds) % 60))
	},

	// ================================= //
    // GET MEDIA INFORMATION 
    // ================================= //
    getInformation: (path) =>{
        return new Promise((resolve,reject)=>{
            RNFFprobe.getMediaInformation(path).then(information => {
                resolve(information);
            })
        });
    },

    // ================================= //
    // WRITE FILE DATA WHEN DOWNLOAD
    // ================================= //
    writeFileData: async(type,json)=> {
		MainService.realmDb.write(() => {
			MainService.realmDb.create(type, {
				_id: MainService.makeid(25),
				...json
			});
		});
        store.dispatch({ type: "UPDATE_UID_TRACK", updateUIDTrack: MainService.makeid(25)});
	},

    // ================================= //
    // CREATE THUMBNAIL FROM VIDEO
    // ================================= //
    createThumbnail: async(path,title)=> {
		return new Promise((resolve,reject)=>{
            RNFFmpeg.executeWithArguments(['-i' ,`${path}`, '-ss', '00:00:01.000',`-vframes`,'1',`${RNFS.DocumentDirectoryPath}/Thumbnails/${title}.png`]).then(result =>{
				resolve(`${RNFS.DocumentDirectoryPath}/Thumbnails/${title}.png`);
			});
        });
	}
};

export default MainService;