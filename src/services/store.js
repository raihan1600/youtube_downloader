import { createStore, combineReducers } from 'redux';
import { initReducer } from '@pages/init.reducer';
import { downloadReducer } from '@pages/download/download.reducer';
import { audioReducer } from '@pages/audio/audio.reducer';
import { googleDriveReducer } from '@pages/google-drive/google-drive.reducer';
import { dropboxReducer } from '@pages/dropbox/dropbox.reducer';
import { videoEditorReducer } from '@pages/video-editor/vider-editor.reducer';

const reducers = combineReducers({
	'init': initReducer,
	'download': downloadReducer,
	'audio': audioReducer,
	'googleDrive': googleDriveReducer,
	'dropbox': dropboxReducer,
	'videoEditor': videoEditorReducer
});

export const store = createStore(
	reducers,
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)