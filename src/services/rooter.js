import React, { Component } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Menucomponent from "@pages/menu/menu.component";

import VideoEditorComponent from "@pages/video-editor/video-editor.component";
import folderComponent from "@pages/folder/folder.component";

import DownloadComponent from "@pages/download/download.component";
import ProfileLoginComponent from "@pages/profile/profile-login.component";

import AudioPlaylistComponent from "@pages/audio/audio-playlist.component";
import AudioListComponent from "@pages/audio/audio-list.component";

import GoogleDriveComponent from "@pages/google-drive/google-drive.component";
import BrowseGalleryComponent from "@pages/browse-gallery/browse-gallery.component";
import DropBoxComponent from "@pages/dropbox/dropbox.component";
import WifiTransferComponent from "@pages/wifi-transfer/wifi-transfer.component";

import MainComponent from "../main.component";
import { SafeAreaView } from "react-native";
import { getDeepLink } from '@services/utilities';

const Stack = createNativeStackNavigator();

class Rooter extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	
	render() {
		
		return (
		<SafeAreaView style={{flex: 1}}>
			<NavigationContainer
				linking={[getDeepLink()]}
				>
				<Stack.Navigator
					initialRouteName="MainComponent"
					screenOptions={{headerShown: false}}
				>
				<Stack.Screen name="MainComponent" component={MainComponent}/>
				
				<Stack.Screen name="Download" component={DownloadComponent} />
				<Stack.Screen name="GoogleDrive" component={GoogleDriveComponent} />
				<Stack.Screen name="Dropbox" component={DropBoxComponent} />
				<Stack.Screen name="Wifi" component={WifiTransferComponent} />
				<Stack.Screen name="BrowseGallery" component={BrowseGalleryComponent} />

                <Stack.Screen name="AudioPlaylist" component={AudioPlaylistComponent} />
                <Stack.Screen name="ListAudio" component={AudioListComponent} />
				
				<Stack.Screen name="VideoEditor" component={VideoEditorComponent}/>

				<Stack.Screen name="Folder" component={folderComponent}/>
				<Stack.Screen name="ProfileLogin" component={ProfileLoginComponent}/>
				</Stack.Navigator>
			</NavigationContainer>
		</SafeAreaView>
		);
	}
}

export default Rooter;
