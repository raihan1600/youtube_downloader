import { Dimensions, StyleSheet } from "react-native";

const mainStyles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor:'#ffffff'
	},
	standardContent: {
        padding: 15,
        flex: 1
	},
	middleContent: {
		flex: 1,
		justifyContent: "center",
        paddingLeft: 15,
        paddingRight: 15
	},
	formGroup:{
		marginBottom: 10
	},
	wrapEditicon:{
        backgroundColor:'#F7F9Fe',
        width:34,
        height:20,
        marginRight:10
    },
	textList:{
		marginLeft:8,
		fontWeight:'500'
	},
	iconFolder:{
        width:36,
        height:30
    },
	iconTopNavigation:{
		width: 26, 
		height: 26,
		marginLeft:5
	},
	wrapFileicon:{
        marginRight:5,
        marginLeft:15
    },
	
});

export default mainStyles;
