import React, { Component } from 'react';
import watch from 'redux-watch';
import MainService from '@services/main';
import { connect } from 'react-redux';
import { store } from '@services/store';
import TrackPlayer, { State, RepeatMode } from 'react-native-track-player';

class PlaylistStore extends Component {
    constructor(props) {
        super(props);
        this.state = {
			playlistIdAudio: this.props.audio.playlistId,
			originTrackList: []
        };
    }

    componentDidMount(){
        setTimeout(()=>{
            this.initAudioPlaylist();
			this.initTrackPlayer();
        }, 1000);

        // ============================= //
        // AUDIO PLAYLIST LISTENER
        // ============================= //
        let watchPlaylistAudio = watch(store.getState, 'audio.updateUIDAudio')
        store.subscribe(watchPlaylistAudio((newVal, oldVal, objectPath) => {
            this.initAudioPlaylist();
        }));

		// ============================= //
        // TRACK LISTENER
        // ============================= //
        let watchTrack = watch(store.getState, 'audio.updateUIDTrack')
        store.subscribe(watchTrack((newVal, oldVal, objectPath) => {
			this.initTrackPlayer();
        }));
		let watchTrackFromPlaylist = watch(store.getState, 'audio.playlistId')
        store.subscribe(watchTrackFromPlaylist((newVal, oldVal, objectPath) => {
			this.setState({playlistIdAudio:newVal},()=>{
				this.initTrackPlayer();
			});
        }));
    }

	convertSeconds(seconds) {
        var convert = function(x) { return (x < 10) ? "0"+x : x; }
        return convert(parseInt(seconds / (60*60))) + ":" +
                convert(parseInt(seconds / 60 % 60)) + ":" +
                convert(seconds % 60)
	}

	// =============================== //
	// INIT TRACK PLAYER
	// =============================== //
	initTrackPlayer = async ()=> {
		let list = [];
		const listAudio = MainService.realmDb.objects("Audios");
		if(this.state.playlistIdAudio){
			const listPlaylistData = MainService.realmDb.objects("PlaylistData");
			const filterPlaylist = listPlaylistData.filtered("playlist_id = '" + this.state.playlistIdAudio + "'");
			if(filterPlaylist.length){
				filterPlaylist.map((x)=>{
					const findAudio = listAudio.filtered("_id = '" + x.media_id + "'");
					if(findAudio.length){
						findAudio.map((x) => {
							list.push({
								id: x['_id'],
								url: `file://${x.path}`,
								artwork: x.thumbnail ? `file://${x.thumbnail}` : "",
								title: x.title,
								artist: x.artist,
								licensed:x.licensed,
								duration:x.duration,
								open: false,
							});
						});
						store.dispatch({ type: "UPDATE_TRACK_AUDIO", trackAudio: list });
						store.dispatch({ type: "UPDATE_ORIGIN_TRACK_AUDIO", originTrackAudio: list });
						this.setState({originTrackList: list});
					}else{
						store.dispatch({ type: "UPDATE_TRACK_AUDIO", trackAudio: [] });
						store.dispatch({ type: "UPDATE_ORIGIN_TRACK_AUDIO", originTrackAudio: [] });
						this.setState({originTrackList: []});
					}
				});
			}else{
				store.dispatch({ type: "UPDATE_TRACK_AUDIO", trackAudio: [] });
				store.dispatch({ type: "UPDATE_ORIGIN_TRACK_AUDIO", originTrackAudio: [] });
				this.setState({originTrackList: []});
			}
		}else{
			if(listAudio.length){
				listAudio.map((x)=>{
					list.push({
						id: x['_id'],
						url: `file://${x.path}`,
						artwork: x.thumbnail ? `file://${x.thumbnail}` : "",
						title: x.title,
						artist: x.artist,
						licensed:x.licensed,
						duration:x.duration,
						open: false,
					});
				});
				store.dispatch({ type: "UPDATE_TRACK_AUDIO", trackAudio: list });
				store.dispatch({ type: "UPDATE_ORIGIN_TRACK_AUDIO", originTrackAudio: list });
				this.setState({originTrackList: list});
			}else{
				store.dispatch({ type: "UPDATE_TRACK_AUDIO", trackAudio: [] });
				store.dispatch({ type: "UPDATE_ORIGIN_TRACK_AUDIO", originTrackAudio: [] });
				this.setState({originTrackList: []});
			}
		}

		TrackPlayer.getQueue().then(resQueu =>{
			if(resQueu.length){
				this.props.audio.trackAudio.map((x)=>{
					let find = resQueu.find((y)=> y.id == x.id);
					if(!find && this.props.audio.playlistId == this.props.audio.playlistIdOnTrack){
						TrackPlayer.add(x);
					}
				});
			}
		});
	}

    // =============================== //
	// INIT AUDIO PLAYLIST
	// =============================== //
	initAudioPlaylist = async ()=> {
		let list = [];
		const listPlaylist = MainService.realmDb.objects("Playlist").filter(
			obj => obj.type === "Audio"
		);
		if(listPlaylist.length){
			listPlaylist.map((x)=>{
				list.push({
					id: x['_id'],
					name: x.name,
					type: "Audio",
					open: false
				});

				if(list.length === listPlaylist.length){
					this.setState({playlistData: list});
					store.dispatch({ 
						type: "UPDATE_PLAYLIST_AUDIO", 
						playlistAudio: list
					});
				}
			});
		}else{
			this.setState({playlistData: list});
            store.dispatch({ 
                type: "UPDATE_PLAYLIST_AUDIO", 
                playlistAudio: list
            });
		}
	}	

    render() {
        return null;
    }
}

function mapStateToProps(state) {
    return {
		audio: state.audio
    }
}
export default connect(mapStateToProps, null, null, { forwardRef: true })(PlaylistStore);
