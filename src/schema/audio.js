const AudioSchema = {
    name: "Audios",
    properties: {
        _id: "string",
        title: "string",
        category: "string",
        category_url: "string",
        artist: "string",
        artist_url: "string",
        licensed: "string",
        path: "string",
        thumbnail: "string",
        duration:"string",
        content_length: "int"
    },
    primaryKey: "_id",
};

export default AudioSchema;