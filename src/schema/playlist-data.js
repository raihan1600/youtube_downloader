const PlaylistDataSchema = {
    name: "PlaylistData",
    properties: {
        _id: "string",
        playlist_id: "string",
        media_id: "string"
    },
    primaryKey: "_id",
};

export default PlaylistDataSchema;