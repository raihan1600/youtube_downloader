const PlaylistSchema = {
    name: "Playlist",
    properties: {
        _id: "string",
        name: "string",
        type: "string"
    },
    primaryKey: "_id",
};

export default PlaylistSchema;